/* underline */
$('.menu__submenu').on('mouseover', function(){
    $(this).prev().addClass('active');
})
$('.menu__submenu').on('mouseout', function(){
    $(this).prev().removeClass('active');
})
// document.onReadyif($('.submenu__item .submenu__item').hasClass('current')) {
//     console.log('hello');
//     $(this).parent('menu__submenu').prev().addClass('active');
// }
/*lang*/
$('#menu-menyu-vybora-yazyka .item__link').click(
    function(){
        $(this).next().toggleClass('active');
    }
)
/* mobile */
$('.header__burger').click(
    function(){
        $('.header__menu').addClass('active');
    }
)

$('.header__menu .menu__item .item__link').click(
    function(){
        $(this).parent().toggleClass('active');
        $(this).next('.menu__submenu').toggleClass('active');
    }
)
$('.header__menu .menu__close').click(
    function(){
        $('.header__menu').removeClass('active');
    }
)
/* fixed header */
$(window).scroll(function() {
    var height = $(window).scrollTop();
    /*Если сделали скролл на 100px задаём новый класс для header*/
    if(height > 20) {
        $('header').addClass('fixed');
    } else {
        /*Если меньше 100px удаляем класс для header*/
        $('header').removeClass('fixed');
    }
});
/* main slider */
$('.slider').slick({
    dots: true,
    dotsClass: "slider__dots container",
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    arrows: false,
    adaptiveHeight: true
});
/* gallery */
$('.gallery__wrapper').slick(
    {
        dots: true,
        speed: 300,
        slidesToShow: 1,
        arrows: false,
        adaptiveHeight: true,
        variableWidth: true
    }
);
window.addEventListener("resize", function() {
    if (window.innerWidth <= 1279) {
        $('.gallery__wrapper').slick('unslick');
        sliderIsLive = false;
    }
    else {
        if (sliderIsLive) {
            $('.gallery__wrapper').slick({
                dots: true,
                speed: 300,
                slidesToShow: 1,
                arrows: false,
                adaptiveHeight: true,
                variableWidth: true
            });
            sliderIsLive = true;
        }
    }
});



/*catalog tabs*/
$('.main.inner').each(function() {
    let ths = $(this);
    ths.find('.tab-item').not(':first').hide();
    ths.find('.menu__item').click(function() {
        ths.find('.menu__item').removeClass('active').eq($(this).index()).addClass('active');
        ths.find('.tab-item').hide().eq($(this).index()).fadeIn()
    }).eq(0).addClass('active');
});


// $('.header__menu .menu__item').click(
//     function(){
//         if ($(this).hasClass('active')) {
//             $(this).removeClass('active');
//             $(this).children('.menu__submenu').removeClass('active');
//             console.log('hi');
//         }
//     }
// )