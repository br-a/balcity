<?php get_header(); ?>
<main class="main inner 123">
    <div class="breadcrumbs">
        <div class="breadcrumbs__wrapper container">
            <a href="<?php echo get_home_url(); ?>">Main</a>
            <span> \ </span>
            <a> <? single_cat_title() ?> </a>
        </div>
    </div>
    <section class="cert container">
        <h1 class="cert__title title__head">
        <? single_cat_title() ?>
        </h1>
        <div class="cert__info">
            <div class="cert__img">
                <img src="<?= get_template_directory_uri() ?>/assets/img/cert-info.png" alt="">
            </div>
        
        <div class="cert__text">
		<?php pll_e('cert_text'); ?>

        </div>
    </div>
    <nav class="cert__menu">
            <ul class="menu__list">
                <li class="menu__item active">
                    All                   
                </li>
                <li class="menu__item">
                The Customs Union Certificates
                </li>
                <li class="menu__item">
                ECE UN approvals 
                </li>
            </ul>
        </nav>
    </section>
        <div class="cert__wrapper">
        <section class="cert__all tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'certificate',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="cert__item">
                    <div class="container">
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <div class="item__text">
                    <?php
                        global $more;
                        $more = 1;

                    $rows = get_field('sertifikaty_ts-en');
                    if ($rows) {
                        foreach ($rows as $row) {
                            $link = $row['sertifikaty_ts-link-en'];
                            if($link) {
                                echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                }
                        }
                    }
                        $rows = get_field('sertifikaty_oon-en');
                        if ($rows) {
                            foreach ($rows as $row) {
                                $link = $row['sertifikaty_oon-en'];
                                if($link) {
                                    echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                    }
                            }
                        }
                        elseif (!get_field('sertifikaty_ts')) {
                            echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                        }

                    ?>

                    </div>
                </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
        </section>
        <section class="cert__oon tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'sertifikaty',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="cert__item">
                <div class="container">
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <div class="item__text">
                    <?php
                                $rows = get_field('sertifikaty_ts-en');
                                if ($rows) {
                                    foreach ($rows as $row) {
                                        $link = $row['sertifikaty_ts-link-en'];
                                        if($link) {
                                            echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                            }
                                            else {
                                                $countEmpty++;
                                            }
                                    }
                                    if(count($rows) == $countEmpty) {
                                        echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                                    }
                                }
                                else {
                                    echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                                }
                                ?>
                        <?php
                        global $more;
                        $more = 1;
                        
                        ?>
                    </div>
                </div>
    </div>

                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
        </section>
        <section class="cert__ts tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'sertifikaty',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); 
            if(get_the_ID() !== 766 && get_the_ID() !== 776 && get_the_ID() !== 773 && get_the_ID() !== 783 ) { 
            ?>
                <div class="cert__item">
                <div class="container">
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;
                        $countEmpty = 0;
                        $rows = get_field('sertifikaty_oon-en');
                        if ($rows) {
                            foreach ($rows as $row) {
                                $link = $row['sertifikaty_oon-en'];
                                if($link) {
                                    echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                } 
                                else {
                                    $countEmpty++;
                                }
                            }
                            if(count($rows) == $countEmpty) {
                                echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                            }
                        }
                        else {
                            echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                        }
                        ?>
                    </div>
                </div>
    </div>
    <? 
                
            } ?>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
    </div>
</main>
<?php get_footer(); ?>
