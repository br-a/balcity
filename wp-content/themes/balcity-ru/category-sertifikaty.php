<?php get_header(); ?>
<main class="main inner">
    <div class="breadcrumbs">
        <div class="breadcrumbs__wrapper container">
            <a href="<?php echo get_home_url(); ?>">Главная</a>
            <span> \ </span>
            <a> <? single_cat_title() ?> </a>
        </div>
    </div>
    <section class="cert container">
        <h1 class="cert__title title__head">
        <? single_cat_title() ?>
        </h1>
        <div class="cert__info">
            <div class="cert__img">
                <img src="<?= get_template_directory_uri() ?>/assets/img/cert-info.png" alt="">
            </div>
        
        <div class="cert__text">
		<?php pll_e('cert_text'); ?>

        </div>
    </div>
    <nav class="cert__menu">
            <ul class="menu__list">
                <li class="menu__item active">
                    Все сертификаты
                </li>
                <li class="menu__item">
                Сертификаты Таможенного Союза
                </li>
                <li class="menu__item">
                Официальное утверждение ЕЭК ООН
                </li>
            </ul>
        </nav>
    </section>
        <div class="cert__wrapper">
        <section class="cert__all tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'sertifikaty',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="cert__item">
                    <div class="container">
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;

                    $rows = get_field('sertifikaty_ts');
                    if ($rows) {
                        foreach ($rows as $row) {
                            $link = $row['sertifikaty_ts-link'];
                            if($link) {
                                echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                }
                        }
                    }
                        $rows = get_field('sertifikaty_oon');
                        if ($rows) {
                            foreach ($rows as $row) {
                                $link = $row['sertifikaty_oon-link'];
                                if($link) {
                                    echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                    }
                            }
                        }
                        elseif (!get_field('sertifikaty_ts')) {
                            echo '<p style="margin-bottom: 30px;">Нет подходящих сертификатов для данной категории</p>';
                        }

                    ?>

                    </div>
                </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
        </section>
        <section class="cert__oon tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'sertifikaty',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="cert__item">
                <div class="container">
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <div class="item__text">
                    <?php
                    $rows = get_field('sertifikaty_ts');
                    if ($rows) {
                        foreach ($rows as $row) {
                            $link = $row['sertifikaty_ts-link'];
                            if($link) {
                                echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                }
                        }
                    }
                    else {
                        echo '<p style="margin-bottom: 30px;">Нет подходящих сертификатов для данной категории</p>';
                    }
    ?>
                        <?php
                        global $more;
                        $more = 1;
                        
                        ?>
                    </div>
                </div>
    </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
        </section>
        <section class="cert__ts tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'sertifikaty',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); 
                if(get_the_ID() !== 492 && get_the_ID() !== 486 && get_the_ID() !== 332 && get_the_ID() !== 489) { 
            ?>
            
                <div class="cert__item">
                <div class="container">
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;
                        $rows = get_field('sertifikaty_oon');
                        $countEmpty = 0;
                        if ($rows) {
                            foreach ($rows as $row) {
                                $link = $row['sertifikaty_oon-link'];
                                if($link) {
                                    echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                    }
                                    else {
                                        $countEmpty++;
                                    }
                            }
                            if(count($rows) == $countEmpty) {
                                echo '<p style="margin-bottom: 30px;">Нет подходящих сертификатов для данной категории</p>';
                            }
                        }
                        else {
                            echo '<p style="margin-bottom: 30px;">Нет подходящих сертификатов для данной категории</p>';
                        }
                        ?>
                    </div>
                </div>
    </div>
                   <? 
                
                } ?>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
    </div>
</main>
<?php get_footer(); ?>
