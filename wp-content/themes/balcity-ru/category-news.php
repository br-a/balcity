<?php get_header(); ?>
<main class="main inner">
    <div class="breadcrumbs">
        <div class="breadcrumbs__wrapper container">
            <a href="<?php echo get_home_url(); ?>">Главная</a>
            <span> \ </span>
            <a><?php the_title(); ?></a>
        </div>
    </div>
    <section class="news container">
        <h1 class="news__title title__head">
            Новости
        </h1>

        <?php $query = new WP_Query(array(
            'category_name' => 'news',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="news__item">
                    <h2 class="item__title"><?php the_title(); ?></h2>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;
                        the_content();
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>

</main>
<?php get_footer(); ?>
