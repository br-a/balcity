<?php
/**
 * balcity.ru functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package balcity.ru
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

if (!function_exists('balcity_ru_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function balcity_ru_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on balcity.ru, use a find and replace
         * to change 'balcity-ru' to the name of your theme in all the template files.
         */
        load_theme_textdomain('balcity-ru', get_template_directory() . '/languages');
        add_theme_support('post-formats', array('aside', 'gallery', 'image', 'video', 'audio'));

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        pll_register_string('Domestic gas bottles', 'Domestic-link');
        pll_register_string('Automotive tanks', 'Automotive-link');
        pll_register_string('Gasholder', 'Gasholder-link');
        pll_register_string('Gas shut off', 'products');
        pll_register_string('productsLink', 'productsLink');
        pll_register_string('Products', 'products');
        pll_register_string('Балсити', 'Balcity');
        pll_register_string('Главная', 'main-page');
        pll_register_string('Главная ссылка', 'main-link');
        pll_register_string('ссылка на лого шапка', 'logo-head');
        pll_register_string('ссылка на лого подвал', 'logo-footer');
        pll_register_string('Сертификаты текст', 'cert_text');
		pll_register_string('Адрес', 'addr');
        pll_register_string('Позвонить', 'call');
        pll_register_string('В каталог', 'catalog');
        pll_register_string('Бытовые баллоны', 'Domestic-gas');
        pll_register_string('Автомобильные баллоны', 'Automotive-tanks');
		pll_register_string('Запорные вентили', 'Gas-shut-off');
        pll_register_string('Газгольдеры', 'Gasholder');
        pll_register_string('Автомобильные баллоны Заголовок', 'Automotive-title');
        pll_register_string('Автомобильные баллоны 1 строка', 'Automotive-row1');
        pll_register_string('Автомобильные баллоны 2 строка', 'Automotive-row2');
        pll_register_string('Автомобильные баллоны 3 строка', 'Automotive-row3');
        pll_register_string('Автомобильные баллоны 4 строка', 'Automotive-row4');
        pll_register_string('Автомобильные баллоны 1 значение', 'Automotive-value1');
        pll_register_string('Автомобильные баллоны 2 значение', 'Automotive-value2');
        pll_register_string('Автомобильные баллоны 3 значение', 'Automotive-value3');
        pll_register_string('Автомобильные баллоны 4 значение', 'Automotive-value4');
        pll_register_string('Автомобильные баллоны текст', 'Automotive-text');
        pll_register_string('Если нет товаров', 'no-product');
        pll_register_string('Заголовок 404', 'title-404');
        pll_register_string('Текст 404', 'text-404');
        pll_register_string('Текст на ссылке 404', 'link-404');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(
            array(
                'menu-1' => esc_html__('Верхнее Меню', 'balcity-ru'),
                'menu-2' => esc_html__('Меню в подвале', 'balcity-ru'),
                'menu-lang' => esc_html__('Меню языка', 'balcity-ru')
            )
        );
        // Изменяет основные параметры меню
        add_filter('wp_nav_menu_args', 'filter_wp_menu_args');
        function filter_wp_menu_args($args)
        {
            if ($args['theme_location'] === 'menu-2') {
                $args['container'] = 'nav';
                $args['container_class'] = 'footer__menu';
                $args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
                $args['menu_class'] = 'menu__list';
                $args['depth'] = '1';
            }
            else if ($args['theme_location'] === 'menu-1') {
                $args['container'] = 'false';
//                $args['container_class'] = 'header__menu';
                $args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
                $args['menu_class'] = 'menu__list';
            }

            return $args;
        }

// Изменяем атрибут id у тега li
        add_filter('nav_menu_item_id', 'filter_menu_item_css_id', 10, 4);
        function filter_menu_item_css_id($menu_id, $item, $args, $depth)
        {
            return $args->theme_location === 'menu-2' || 'menu-1' ? '' : $menu_id;
        }

// Изменяем атрибут class у тега li
        add_filter('nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4);
        function filter_nav_menu_css_classes($classes, $item, $args, $depth)
        {
            if ($args->theme_location === 'menu-2' || 'menu-1') {
                $classes = [
                     $depth == 1 ? 'submenu__item' : 'menu__item'
                ];
            }

            return $classes;
        }


        add_action('after_setup_theme', 'balcity_ru_setup');

        function mt_add_pages() {
            add_menu_page('admin-menu', 'Новости', 0, 'https://balcity.ru/wp-admin/edit.php?s&post_status=all&post_type=post&action=-1&m=0&cat=23&filter_action=%D0%A4%D0%B8%D0%BB%D1%8C%D1%82%D1%80&paged=1&action2=-1');
            add_menu_page('admin-menu', 'Продукция', 0, 'https://balcity.ru/wp-admin/edit.php?s&post_status=all&post_type=post&action=-1&m=0&cat=71&filter_action=%D0%A4%D0%B8%D0%BB%D1%8C%D1%82%D1%80&paged=1&action2=-1');
            add_menu_page('admin-menu', 'Сертификаты', 0, 'https://balcity.ru/wp-admin/edit.php?s&post_status=all&post_type=post&action=-1&m=0&cat=42&filter_action=%D0%A4%D0%B8%D0%BB%D1%8C%D1%82%D1%80&paged=1&action2=-1');
			add_menu_page('admin-menu', 'Сертификаты(en)', 0, 'https://balcity.ru/wp-admin/edit.php?s&post_status=all&post_type=post&action=-1&m=0&cat=99&filter_action=%D0%A4%D0%B8%D0%BB%D1%8C%D1%82%D1%80&paged=1&action2=-1');
        }
        add_action('admin_menu', 'mt_add_pages');


        // Изменяем атрибут class у тега li родителя
        // add_filter( 'nav_menu_css_class', 'check_for_submenu', 10, 2);
        // function check_for_submenu($classes, $item) {
        //     global $wpdb;
        //     $has_children = $wpdb->get_var("SELECT COUNT(meta_id) FROM wp_postmeta WHERE meta_key='_menu_item_menu_item_parent' AND meta_value='".$item->ID."'");
        //     if ($has_children > 0) array_push($classes,'current'); // add the class dropdown to the current list
        //     return $classes;
        // }

// Изменяет класс у вложенного ul
//        add_filter('nav_menu_submenu_css_class', 'filter_nav_menu_submenu_css_class', 10, 3);
//        function filter_nav_menu_submenu_css_class($classes, $args, $depth)
//        {
//            if ($args->theme_location === 'menu-1') {
//                $classes = [
//                    'submenu__list'
//                ];
//            }
//
//            return $classes;
//        }
        remove_filter('the_content', 'wpautop');
        remove_filter('the_excerpt', 'wpautop');

// ДОбавляем классы ссылкам
        add_filter('nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4);
        function filter_nav_menu_link_attributes($atts, $item, $args, $depth)
        {
            if ($args->theme_location === 'menu-2' || 'menu-1') {
                $atts['class'] = 'item__link';

                if ($item->current) {
                    $atts['class'] .= ' current';
                }
            }

            return $atts;
        }
        // navWalker
        class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu
        {
            function start_lvl( &$output, $depth = 1, $args = array() ) {
                $indent = str_repeat("\t", $depth);
                $output .= "\n$indent<nav class='menu__submenu'><ul class='submenu__list'>\n";
            }
            function end_lvl( &$output, $depth = 1, $args = array() ) {
                $indent = str_repeat("\t", $depth);
                $output .= "$indent</ul></nav>\n";
            }
        }

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
            )
        );

        // Set up the WordPress core custom background feature.
        add_theme_support(
            'custom-background',
            apply_filters(
                'balcity_ru_custom_background_args',
                array(
                    'default-color' => 'ffffff',
                    'default-image' => '',
                )
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support(
            'custom-logo',
            array(
                'flex-width' => true,
                'flex-height' => true,
                'unlink-homepage-logo' => true,
                'class'    => 'logo__img',
            )
        );
    }
endif;
add_action('after_setup_theme', 'balcity_ru_setup');


/*cert*/
function get_custom_cat_template($single_template) {
    global $post;

    if ( in_category( 'sertifikaty' )) {
        $single_template = dirname( __FILE__ ) . '/single-sertifikaty.php';
    }
    return $single_template;
}

add_filter( "single_template", "get_custom_cat_template" ) ;
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function balcity_ru_content_width()
{
    $GLOBALS['content_width'] = apply_filters('balcity_ru_content_width', 640);
}

add_action('after_setup_theme', 'balcity_ru_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function balcity_ru_widgets_init()
{
    register_sidebar(
        array(
            'name' => esc_html__('Sidebar', 'balcity-ru'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here.', 'balcity-ru'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
}

add_action('widgets_init', 'balcity_ru_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function balcity_ru_scripts()
{
    wp_enqueue_style('balcity-ru-style', get_stylesheet_uri(), array(), _S_VERSION);
//    wp_enqueue_style( 'balcity-ru-fonts', 'https://fonts.googleapis.com' );
//    wp_enqueue_style( 'balcity-ru-fonts2','https://fonts.gstatic.com');
    wp_enqueue_style('balcity-ru-fonts3', 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap');
    wp_enqueue_style('balcity-ru-slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css');
    wp_enqueue_style('balcity-ru-main', get_template_directory_uri() . '/assets/css/style.min.css');
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js');
    wp_enqueue_script('jquery');
    wp_enqueue_script('balcity-ru-slickjs', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('balcity-ru-main', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery', 'balcity-ru-slickjs'), _S_VERSION, true);
}

add_action('wp_enqueue_scripts', 'balcity_ru_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

