<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package balcity.ru
 */

get_header();
?>
    <main class="main inner">
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
                <span> \ </span>
                <a><?php the_title(); ?></a>
            </div>
        </div>
        <section class="contacts container">
            <h1 class="contacts__title title__head">
                <?php the_title(); ?>
            </h1>
            <!-- .entry-header -->

            <?php balcity_ru_post_thumbnail(); ?>

            <div class="contacts__wrapper">
                <div class="contacts__info">
                    <?php
                    the_content();

                    wp_link_pages(
                        array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'balcity-ru' ),
                            'after'  => '</div>',
                        )
                    );
                    ?>
                </div>
                <div class="contacts__map">
                    <?php echo do_shortcode('[yamap center="55.7204,37.6308" height="22rem" controls="zoomControl;routeButtonControl" zoom="15" type="yandex#map" scrollzoom="0"][yaplacemark  name="Балсити" coord="55.7201,37.6299" icon="islands#dotIcon" color="#BE2A2A"][/yamap]'); ?>

                </div>
            </div><!-- .entry-content -->
        </section>
        <div class="feed container">
            <h2 class="feed__title">Contact us</h2>
            <?php echo do_shortcode('[contact-form-7 id="1166" title="Contact form 2"]'); ?>
        </div>
    </main><!-- #main -->

<?php

get_footer();