<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package balcity.ru
 */

get_header();
if ( is_page(87)) {
?>

<main class="main">
        <section class="slider">
        <?
        $slajder = get_field('slajder-en', 83);
        foreach($slajder as $item => $value) { ?>
            <div class="slider__item" style="background: url(<? echo $slajder[$item]['izobrazhenie']['url']; ?>);background-size: cover; background-position: center; background-repeat: no-repeat;">
                <div class="item__info container">
                    <h2 class="item__title">
                       <? echo $slajder[$item]['zagolovok']; ?>
                    </h2>
                    <div class="item__text">
                        <? echo $slajder[$item]['tekst']; ?>
                    </div>
                </div>
            </div> 
            <? } ?>       
            <!-- <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-1.jpg);background-size: cover; background-position: center; background-repeat: no-repeat;">
                <div class="item__info container">
                    <h2 class="item__title">
                        Экологичный
                        и безопасный
                    </h2>
                    <div class="item__text">
                        «Балсити» — признанный лидер на российском рынке автомобильных баллонов для сжиженного углеводородного газа
                    </div>
                </div>
            </div>
            <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-2.png);background-size: cover; background-position: center; background-repeat: no-repeat;">
                <div class="item__info container">
                    <h2 class="item__title">
                        Ваш поставщик
                        решений для сжиженного
                        углеводородного газа
                    </h2>
                    <div class="item__text"></div>
                </div>
            </div>
            <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-4.png); background-size: cover; background-position: center; background-repeat: no-repeat;">
                <div class="item__info container">
                    <h2 class="item__title">
                        Сделано с любовью
                        в России
                    </h2>
                    <div class="item__text"></div>
                </div>
            </div> -->
        </section>
        <section class="catalog">
            <div class="catalog__wrapper container">
                <div class="catalog__item">
                    <h3 class="item__title">
                    <?php pll_e('Domestic-gas'); ?>
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/tanks.webp" alt="" class="item__img">
                    <a href="<?php pll_e('Domestic-link'); ?>" class="item__link">
                    <?php pll_e('catalog'); ?>
                    </a>
                </div>
                <div class="catalog__item">
                    <h3 class="item__title">
                    <?php pll_e('Automotive-tanks'); ?>
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/cylinders.webp" alt="" class="item__img">
                    <a href="<?php pll_e('Automotive-link'); ?>" class="item__link">
                    <?php pll_e('catalog'); ?>
                    </a>
                </div>
                <div class="catalog__item">
                    <h3 class="item__title">
                    <?php pll_e('Gasholder'); ?>
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/gas.webp" alt="" class="item__img">
                    <a href="<?php pll_e('Gasholder-link'); ?>" class="item__link">
                    <?php pll_e('catalog'); ?>
                    </a>
                </div>
                <div class="catalog__item">
                    <h3 class="item__title">
                    <?php pll_e('Gas-shut-off'); ?>
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/arm.webp" alt="" class="item__img">
                    <a href="<?php pll_e('Gas-link'); ?>" class="item__link">
                    <?php pll_e('catalog'); ?>
                    </a>
                </div>
            </div>
        </section>
        <section class="quality container">
            <?php
            $homeTekstUp = get_field('homeTekstUp-en',83);
            if ($homeTekstUp["homeTekstWhite"]) { 
                
                ?>
            <img src="<? echo $homeTekstUp["kartinka"]['sizes']['large']; } ?>" alt="" class="quality__img">
            <!-- <img src="<?= get_template_directory_uri() ?>/assets/img/quality.webp" alt="" class="quality__img">  -->
            <div class="quality__text">
            <?php 
               
            if ($homeTekstUp["homeTekstWhite"]) { 
            echo $homeTekstUp["homeTekstWhite"]; 
            }
            ?>
                <!-- <p>ООО «Балсити» опирается на опыт и сложившиеся традиции в области разработки, производства и эксплуатации баллонов и ёмкостей для сжиженного углеводородного газа (пропана, бутана и их смесей) и занимает лидирующее положение на российском рынке.
                </p>
                <p>
                    Наша компания имеет <a href="">Разрешение Ростехнадзора РФ</a> на применение и сертификаты соответствия на производимую продукцию, серийно изготавливает и поставляет потребителям автомобильные баллоны, ёмкости для хранения СУГ, бытовые баллоны, а также запорные вентили к ним.
                </p>
                <p>
                    Автомобильные баллоны «Балсити» сертифицированы в соответствии с международными <a href="">Правилами ЕЭК ООН 67–01</a>.
                </p> -->
            </div>
        </section>
        <section class="choose container">
            <h2 class="choose__title">WE ARE CHOSEN:</h2>
            <ul class="choose__list">
            <?php $group_fields = get_field('choose-en',83); 
            ?>

<?php if ($group_fields) { ?>

    <?php foreach ($group_fields as $key => $item) { ?>
        <?php if ($item) { ?>
            <li class="choose__item"><img src="<?php echo esc_url($group_fields[$key]['sizes']['large']); ?>"
                    alt="<?php echo esc_attr($group_fields[$key]['alt']); ?>"
                    class="item__img"
                >
        <?php } ?>
    <?php } ?>

<?php } ?>
                <!-- <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/1.webp" alt="" class="item__img"></li>
                <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/2.webp" alt="" class="item__img"></li>
                <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/3.webp" alt="" class="item__img"></li> -->
            </ul>
            <div class="choose__text">
            <?php $chooseText = get_field('chooseText-en',83);
            if ($chooseText) { 
            echo $chooseText; 
            }
            ?>
            </div>
        </section>
        <section class="goods">
            <h2 class="goods__title">
                <!-- «БАЛСИТИ» в цифрах: -->
                <?php
                $video= get_field('tekst_na_video-en',83);
                echo $video['zagolovok'];
                ?>
            </h2>
            <div class="goods__wrapper container">
                <div class="goods__item">
                    <div class="item__count">
                        <!-- 15<span> лет</span> -->
                        <?php
                            echo $video['1_kolonka_-_zagolovok'];
                        ?>
                </div>
                    <div class="item__text">
                        <!-- производим баллоны и ёмкости для сжиженного
                        углеводородного газа -->
                        <?php
                            echo $video['1_kolonka_-_tekst'];
                        ?>
                     </div>
                </div>
                <div class="goods__item">
                    <div class="item__count">
                        <!-- 150 -->
                    <?php
                            echo $video['2_kolonka_-_zagolovok'];
                        ?>
                    </div>
                    <div class="item__text">
                                        <!-- опытных
                        специалистов -->
                        <?php
                            echo $video['2_kolonka_-_tekst'];
                        ?>
                    </div>
                </div>
                <div class="goods__item">
                    <div class="item__count">
                        <!-- 7000 <span>м<sup>2</sup></span> -->
                        <?php
                            echo $video['3_kolonka_-_zagolovok'];
                        ?>
                    </div>
                    <div class="item__text">
                                        <!-- производственных
                        площадей -->
                        <?php
                            echo $video['3_kolonka_-_tekst'];
                        ?>
                    </div>
                </div>
                <div class="goods__item">
                    <div class="item__count">
                        <!-- 2 <span>млн</span> -->
                        <?php
                            echo $video['4_kolonka_-_zagolovok'];
                        ?>
                    </div>
                    <div class="item__text">
                        <!-- произведённых
                        ёмкостей объёмом
                        от 30 до 10 000 л -->
                        <?php
                            echo $video['4_kolonka_-_tekst'];
                        ?>
                    </div>
                </div>
            </div>
            <video autoplay muted loop class="goods__video">
                <source class="goods__info">
                <source src="<?= get_template_directory_uri() ?>/assets/videoplayback.mp4" type="video/mp4">
            </video>
        </section>
        <section class="about container">
        <?
                    $greyOne= get_field('videnie_tekst-en',83);
                    $greyTwo= get_field('vision_tekst-en',83);
                    ?>
            <!-- <img src="<?= get_template_directory_uri() ?>/assets/img/lpg.webp" alt="" class="about__lpg"> -->
            <img src="<? echo $greyOne["kartinka"]['sizes']['large']; ?>" alt="" class="about__lpg">
            <div class="about__vision">
                <div class="vision__text">
                    <h3 class="vision__title">
                        <?
                        if ($greyOne["zagolovok"]) { 
                            echo $greyOne["zagolovok"];
                        }
                        ?>
                    </h3>
                    <?
                        if ($greyOne["tekst"]) { 
                            echo $greyOne["tekst"];
                        }
                        ?>
                </div>
            </div>
            <img src="<? echo $greyTwo["kartinka"]['sizes']['large']; ?>" alt="" class="about__worker">
            <!-- <img src="<?= get_template_directory_uri() ?>/assets/img/worker.webp" alt="" class="about__worker"> -->
            <div class="about__mission">
                <div class="mission__text">
                    <h3 class="mission__title">
                    <?
                        if ($greyTwo["zagolovok"]) { 
                            echo $greyTwo["zagolovok"];
                        }
                        ?>
                    </h3>
                    <?
                        if ($greyTwo["tekst"]) { 
                            echo $greyTwo["tekst"];
                        }
                        ?>
                </div>
            </div>
        </section>
        <section class="advantage container">
            <h2 class="advantage__title title">Advantages «Balcity»</h2>
            <div class="advantage__wrapper">
                <div class="advantage__item exp">
                    <?php
                    $prem = get_field('sekicziya_preimushhestva-en',83);
                    ?>
                    <h3 class="item__title">
                        
                        <?
                        if ($prem["krasnyj_blok_-_zagolovok"]) { 
                            echo $prem["krasnyj_blok_-_zagolovok"];
                        }
                        ?>
                    </h3>
                    <div class="item__text">
                    <?
                        if ($prem["opyt"]) { 
                            echo $prem["opyt"];
                        }
                        ?>
                    </div>
                </div>
                <div class="advantage__item iso">
                    <h3 class="item__title">
                        
                        <?
                        if ($prem["temno-seryj_blok_-_zagolovok"]) { 
                            echo $prem["temno-seryj_blok_-_zagolovok"];
                        }
                        ?>
                    </h3>
                    <div class="item__text">
                    <?
                        if ($prem["kachestvo"]) { 
                            echo $prem["kachestvo"];
                        }
                        ?>
                        <!-- Контроль качества происходит на всех этапах производства, от заказа сырья и материалов до передачи на склад готовой продукции.
                        Учитывая специфику выпускаемых изделий, приоритетной задачей в работе БАЛСИТИ является обеспечение надлежащего качества продукции. Все изделия в процессе изготовления проходят полный комплекс испытаний и подвергаются 100% контролю (рентгеновский, гидравлический и пневматический).
                        <p>
                            Система менеджмента качества предприятия применительно к производству, продаже и поставке продукции сертифицирована на соответствие Европейскому стандарту ISO 9001–2015.
                            «Балсити» - первая компания-производитель из стран СНГ, получившая Европейские сертификаты ECE R 67–01 на свои автомобильные баллоны.
                        </p> -->
                    </div>
                </div>
            </div>
            <div class="advantage__wrapper">
                <div class="advantage__item tech">
                    <h3 class="item__title">
                        
                        <?
                        if ($prem["svetlo-seryj_blok_-_zagolovok"]) { 
                            echo $prem["svetlo-seryj_blok_-_zagolovok"];
                        }
                        ?>
                    </h3>
                    <div class="item__text">
                    <?
                        if ($prem["tehnologii"]) { 
                            echo $prem["tehnologii"];
                        }
                        ?>
                        <!-- Продукция ООО «Балсити» производится на высокотехнологичном итальянском оборудовании.
                        ООО «Балсити» уделяет особое внимание модернизации производства, что позволяет удовлетворять потребности потребителя и обеспечивать высокое качество продукции.
                        <p>
                            Вот уже на протяжении 15 лет ООО «Балсити» сотрудничает с итальянскими компаниями-поставщиками оборудования для металлообработки. Среди них такие имена, как FACCIN S.p.A., GALDABINI SPA, MECOME Srl, Roccia Srl, MG Srl, GNUTTI Transfer S.p.A, Mecolpress S.p.A., Lucas Srl. Работники нашего предприятия имеют возможность перенимать опыт иностранных коллег.
                            Инвестируя в передовые технологии, мы инвестируем в удовлетворение потребителя от нашей продукции.
                        </p> -->
                    </div>
                </div>
                <div class="advantage__item eco">
                    <h3 class="item__title">
                        
                        <?
                        if ($prem["green_-_zagolovok"]) { 
                            echo $prem["green_-_zagolovok"];
                        }
                        ?>
                    </h3>
                    <div class="item__text">
                    <?
                        if ($prem["ekologichnost"]) { 
                            echo $prem["ekologichnost"];
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
<!--	<main id="primary" class="site-main">-->
<!---->
<!--		--><?php
//		if ( have_posts() ) :
//
//			if ( is_home() && ! is_front_page() ) :
//				?>
<!--				<header>-->
<!--					<h1 class="page-title screen-reader-text">--><?php //single_post_title(); ?><!--</h1>-->
<!--				</header>-->
<!--				--><?php
//			endif;
//
//			/* Start the Loop */
//			while ( have_posts() ) :
//				the_post();
//
//				/*
//				 * Include the Post-Type-specific template for the content.
//				 * If you want to override this in a child theme, then include a file
//				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
//				 */
//				get_template_part( 'template-parts/content', get_post_type() );
//
//			endwhile;
//
//			the_posts_navigation();
//
//		else :
//
//			get_template_part( 'template-parts/content', 'none' );
//
//		endif;
//		?>

<!--	</main> #main -->
<? } elseif(is_page(83)) { ?>
    <main class="main">
<section class="slider">
<?
$slajder = get_field('slajder', 83);
// var_dump($slajder)
foreach($slajder as $item => $value) { ?>
    <div class="slider__item" style="background: url(<? echo $slajder[$item]['izobrazhenie']['url']; ?>);background-size: cover; background-position: center; background-repeat: no-repeat;">
        <div class="item__info container">
            <h2 class="item__title">
               <? echo $slajder[$item]['zagolovok']; ?>
            </h2>
            <div class="item__text">
                <? echo $slajder[$item]['tekst']; ?>
            </div>
        </div>
    </div> 
    <? } ?>       
    <!-- <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-1.jpg);background-size: cover; background-position: center; background-repeat: no-repeat;">
        <div class="item__info container">
            <h2 class="item__title">
                Экологичный
                и безопасный
            </h2>
            <div class="item__text">
                «Балсити» — признанный лидер на российском рынке автомобильных баллонов для сжиженного углеводородного газа
            </div>
        </div>
    </div>
    <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-2.png);background-size: cover; background-position: center; background-repeat: no-repeat;">
        <div class="item__info container">
            <h2 class="item__title">
                Ваш поставщик
                решений для сжиженного
                углеводородного газа
            </h2>
            <div class="item__text"></div>
        </div>
    </div>
    <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-4.png); background-size: cover; background-position: center; background-repeat: no-repeat;">
        <div class="item__info container">
            <h2 class="item__title">
                Сделано с любовью
                в России
            </h2>
            <div class="item__text"></div>
        </div>
    </div> -->
</section>
<section class="catalog">
    <div class="catalog__wrapper container">
        <div class="catalog__item">
            <h3 class="item__title">
            <?php pll_e('Domestic-gas'); ?>
            </h3>
            <img src="<?= get_template_directory_uri() ?>/assets/img/tanks.webp" alt="" class="item__img">
            <a href="/category/produkcziya/bytovye-ballony/" class="item__link">
            <?php pll_e('catalog'); ?>
            </a>
        </div>
        <div class="catalog__item">
            <h3 class="item__title">
            <?php pll_e('Automotive-tanks'); ?>
            </h3>
            <img src="<?= get_template_directory_uri() ?>/assets/img/cylinders.webp" alt="" class="item__img">
            <a href="/category/produkcziya/avtomobilnye-ballony/" class="item__link">
            <?php pll_e('catalog'); ?>
            </a>
        </div>
        <div class="catalog__item">
            <h3 class="item__title">
            <?php pll_e('Gasholder'); ?>
            </h3>
            <img src="<?= get_template_directory_uri() ?>/assets/img/gas.webp" alt="" class="item__img">
            <a href="/category/produkcziya/gazgoldery/" class="item__link">
            <?php pll_e('catalog'); ?>
            </a>
        </div>
        <div class="catalog__item">
            <h3 class="item__title">
            <?php pll_e('Gas-shut-off'); ?>
            </h3>
            <img src="<?= get_template_directory_uri() ?>/assets/img/arm.webp" alt="" class="item__img">
            <a href="/category/produkcziya/zapornye-ventili-dlya-gaza/" class="item__link">
            <?php pll_e('catalog'); ?>
            </a>
        </div>
    </div>
</section>
<section class="quality container">
    <?php
    $homeTekstUp = get_field('homeTekstUp',83);
    if ($homeTekstUp["homeTekstWhite"]) { 
        
        ?>
    <img src="<? echo $homeTekstUp["kartinka"]['sizes']['large']; } ?>" alt="" class="quality__img">
    <!-- <img src="<?= get_template_directory_uri() ?>/assets/img/quality.webp" alt="" class="quality__img">  -->
    <div class="quality__text">
    <?php 
       
    if ($homeTekstUp["homeTekstWhite"]) { 
    echo $homeTekstUp["homeTekstWhite"]; 
    }
    ?>
        <!-- <p>ООО «Балсити» опирается на опыт и сложившиеся традиции в области разработки, производства и эксплуатации баллонов и ёмкостей для сжиженного углеводородного газа (пропана, бутана и их смесей) и занимает лидирующее положение на российском рынке.
        </p>
        <p>
            Наша компания имеет <a href="">Разрешение Ростехнадзора РФ</a> на применение и сертификаты соответствия на производимую продукцию, серийно изготавливает и поставляет потребителям автомобильные баллоны, ёмкости для хранения СУГ, бытовые баллоны, а также запорные вентили к ним.
        </p>
        <p>
            Автомобильные баллоны «Балсити» сертифицированы в соответствии с международными <a href="">Правилами ЕЭК ООН 67–01</a>.
        </p> -->
    </div>
</section>
<section class="choose container">
    <h2 class="choose__title">НАС ВЫБИРАЮТ:</h2>
    <ul class="choose__list">
    <?php $group_fields = get_field('choose',83); 
    ?>

<?php if ($group_fields) { ?>

<?php foreach ($group_fields as $key => $item) { ?>
<?php if ($item) { ?>
    <li class="choose__item"><img src="<?php echo esc_url($group_fields[$key]['sizes']['large']); ?>"
            alt="<?php echo esc_attr($group_fields[$key]['alt']); ?>"
            class="item__img"
        >
<?php } ?>
<?php } ?>

<?php } ?>
        <!-- <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/1.webp" alt="" class="item__img"></li>
        <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/2.webp" alt="" class="item__img"></li>
        <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/3.webp" alt="" class="item__img"></li> -->
    </ul>
    <div class="choose__text">
    <?php $chooseText = get_field('chooseText',83);
    if ($chooseText) { 
    echo $chooseText; 
    }
    ?>
    </div>
</section>
<section class="goods">
    <h2 class="goods__title">
        <!-- «БАЛСИТИ» в цифрах: -->
        <?php
        $video= get_field('tekst_na_video',83);
        echo $video['zagolovok'];
        ?>
    </h2>
    <div class="goods__wrapper container">
        <div class="goods__item">
            <div class="item__count">
                <!-- 15<span> лет</span> -->
                <?php
                    echo $video['1_kolonka_-_zagolovok'];
                ?>
        </div>
            <div class="item__text">
                <!-- производим баллоны и ёмкости для сжиженного
                углеводородного газа -->
                <?php
                    echo $video['1_kolonka_-_tekst'];
                ?>
             </div>
        </div>
        <div class="goods__item">
            <div class="item__count">
                <!-- 150 -->
            <?php
                    echo $video['2_kolonka_-_zagolovok'];
                ?>
            </div>
            <div class="item__text">
                                <!-- опытных
                специалистов -->
                <?php
                    echo $video['2_kolonka_-_tekst'];
                ?>
            </div>
        </div>
        <div class="goods__item">
            <div class="item__count">
                <!-- 7000 <span>м<sup>2</sup></span> -->
                <?php
                    echo $video['3_kolonka_-_zagolovok'];
                ?>
            </div>
            <div class="item__text">
                                <!-- производственных
                площадей -->
                <?php
                    echo $video['3_kolonka_-_tekst'];
                ?>
            </div>
        </div>
        <div class="goods__item">
            <div class="item__count">
                <!-- 2 <span>млн</span> -->
                <?php
                    echo $video['4_kolonka_-_zagolovok'];
                ?>
            </div>
            <div class="item__text">
                <!-- произведённых
                ёмкостей объёмом
                от 30 до 10 000 л -->
                <?php
                    echo $video['4_kolonka_-_tekst'];
                ?>
            </div>
        </div>
    </div>
    <video autoplay muted loop class="goods__video">
        <source class="goods__info">
        <source src="<?= get_template_directory_uri() ?>/assets/videoplayback.mp4" type="video/mp4">
    </video>
</section>
<section class="about container">
<?
            $greyOne= get_field('videnie_tekst',83);
            $greyTwo= get_field('vision_tekst',83);
            ?>
    <!-- <img src="<?= get_template_directory_uri() ?>/assets/img/lpg.webp" alt="" class="about__lpg"> -->
    <img src="<? echo $greyOne["kartinka"]['sizes']['large']; ?>" alt="" class="about__lpg">
    <div class="about__vision">
        <div class="vision__text">
            <h3 class="vision__title">
                <?
                if ($greyOne["zagolovok"]) { 
                    echo $greyOne["zagolovok"];
                }
                ?>
            </h3>
            <?
                if ($greyOne["tekst"]) { 
                    echo $greyOne["tekst"];
                }
                ?>
        </div>
    </div>
    <img src="<? echo $greyTwo["kartinka"]['sizes']['large']; ?>" alt="" class="about__worker">
    <!-- <img src="<?= get_template_directory_uri() ?>/assets/img/worker.webp" alt="" class="about__worker"> -->
    <div class="about__mission">
        <div class="mission__text">
            <h3 class="mission__title">
            <?
                if ($greyTwo["zagolovok"]) { 
                    echo $greyTwo["zagolovok"];
                }
                ?>
            </h3>
            <?
                if ($greyTwo["tekst"]) { 
                    echo $greyTwo["tekst"];
                }
                ?>
        </div>
    </div>
</section>
<section class="advantage container">
    <h2 class="advantage__title title">Преимущества «Балсити»</h2>
    <div class="advantage__wrapper">
        <div class="advantage__item exp">
            <?php
            $prem = get_field('sekicziya_preimushhestva',83);
            ?>
            <h3 class="item__title">
                
                <?
                if ($prem["krasnyj_blok_-_zagolovok"]) { 
                    echo $prem["krasnyj_blok_-_zagolovok"];
                }
                ?>
            </h3>
            <div class="item__text">
            <?
                if ($prem["opyt"]) { 
                    echo $prem["opyt"];
                }
                ?>
            </div>
        </div>
        <div class="advantage__item iso">
            <h3 class="item__title">
                
                <?
                if ($prem["temno-seryj_blok_-_zagolovok"]) { 
                    echo $prem["temno-seryj_blok_-_zagolovok"];
                }
                ?>
            </h3>
            <div class="item__text">
            <?
                if ($prem["kachestvo"]) { 
                    echo $prem["kachestvo"];
                }
                ?>
                <!-- Контроль качества происходит на всех этапах производства, от заказа сырья и материалов до передачи на склад готовой продукции.
                Учитывая специфику выпускаемых изделий, приоритетной задачей в работе БАЛСИТИ является обеспечение надлежащего качества продукции. Все изделия в процессе изготовления проходят полный комплекс испытаний и подвергаются 100% контролю (рентгеновский, гидравлический и пневматический).
                <p>
                    Система менеджмента качества предприятия применительно к производству, продаже и поставке продукции сертифицирована на соответствие Европейскому стандарту ISO 9001–2015.
                    «Балсити» - первая компания-производитель из стран СНГ, получившая Европейские сертификаты ECE R 67–01 на свои автомобильные баллоны.
                </p> -->
            </div>
        </div>
    </div>
    <div class="advantage__wrapper">
        <div class="advantage__item tech">
            <h3 class="item__title">
                
                <?
                if ($prem["svetlo-seryj_blok_-_zagolovok"]) { 
                    echo $prem["svetlo-seryj_blok_-_zagolovok"];
                }
                ?>
            </h3>
            <div class="item__text">
            <?
                if ($prem["tehnologii"]) { 
                    echo $prem["tehnologii"];
                }
                ?>
                <!-- Продукция ООО «Балсити» производится на высокотехнологичном итальянском оборудовании.
                ООО «Балсити» уделяет особое внимание модернизации производства, что позволяет удовлетворять потребности потребителя и обеспечивать высокое качество продукции.
                <p>
                    Вот уже на протяжении 15 лет ООО «Балсити» сотрудничает с итальянскими компаниями-поставщиками оборудования для металлообработки. Среди них такие имена, как FACCIN S.p.A., GALDABINI SPA, MECOME Srl, Roccia Srl, MG Srl, GNUTTI Transfer S.p.A, Mecolpress S.p.A., Lucas Srl. Работники нашего предприятия имеют возможность перенимать опыт иностранных коллег.
                    Инвестируя в передовые технологии, мы инвестируем в удовлетворение потребителя от нашей продукции.
                </p> -->
            </div>
        </div>
        <div class="advantage__item eco">
            <h3 class="item__title">
                
                <?
                if ($prem["green_-_zagolovok"]) { 
                    echo $prem["green_-_zagolovok"];
                }
                ?>
            </h3>
            <div class="item__text">
            <?
                if ($prem["ekologichnost"]) { 
                    echo $prem["ekologichnost"];
                }
                ?>
            </div>
        </div>
    </div>
</section>
</main> 
<? } else {


?>
    <main class="main inner">
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
                <span> \ </span>
                <a><?php the_title(); ?></a>
            </div>
        </div>
        <section class="gallery container">
            <h1 class="gallery__title title__head">
                <?php the_title(); ?>
            </h1>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
        </section>
	</main><!-- #main -->

<?php
}
get_footer();
