<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package balcity.ru
 */

get_header();
?>
<?
if( in_category( 'gazgoldery' )){ ?>
<main class="main inner">
<div class="breadcrumbs">
    <div class="breadcrumbs__wrapper container">
        <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
        <span> \ </span>
        <a href="/produkcziya/">Продукция</a>
        <span> \ </span>
        <a>Газгольдеры</a>
    </div>
</div>
<section class="container">
    <h1 class="catalog__title title__head">
        Газгольдеры
    </h1>

    <nav class="autotank__menu">
        <ul class="menu__list">
            <li class="menu__item active">
            Минигазгольдеры
            </li>
            <li class="menu__item">
            Подземные газгольдеры
            </li>
        </ul>
    </nav>
</section>
<div class="container">
    <section class="autotank__tor tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'minigazgoldery',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="catalog__item">
                    <? if (!empty(get_the_title())) { ?>
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <? } ?>
                    <div class="item__img">
                    <?php echo get_the_post_thumbnail($page->ID); 
                        if(get_field('schemes')){
        echo get_field('schemes', false, false);
    } 
    ?>
                    <?php $image = get_field('shemy_kartinka');
                            $oon = get_field('ssylka_sertifikaty_oon-en');
                            $ts = get_field('ssylka_sertifikaty_ts-en');
                    if($image) {
                        echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                    }
                    ?>
                    </div>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;
                        the_content();
                        ?>
                        <?php 
                            $table = get_field('tablicza');
                        if ( ! empty ( $table ) ) {

echo '<table border="0">';

    if ( ! empty( $table['caption'] ) ) {

        echo '<caption>' . $table['caption'] . '</caption>';
    }

    if ( ! empty( $table['header'] ) ) {

        echo '<thead>';

            echo '<tr>';

                foreach ( $table['header'] as $th ) {

                    echo '<th>';
                        echo $th['c'];
                    echo '</th>';
                }

            echo '</tr>';

        echo '</thead>';
    }

    echo '<tbody>';

        foreach ( $table['body'] as $tr ) {

            echo '<tr>';

                foreach ( $tr as $td ) {

                    echo '<td>';
                        echo $td['c'];
                    echo '</td>';
                }

            echo '</tr>';
        }

    echo '</tbody>';

echo '</table>';

                            if (get_field('opisanie_tovara-en')) {
                                echo get_field('opisanie_tovara-en', false, false);
                            }
                            if($oon["url"]) {
                                echo '<a class="oon" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                            }
                            if($ts["url"]) {
                                echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                            }
}
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
    <section class="autotank__cylindrical tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'podzemnye-gazgoldery',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="catalog__item">
                    <? if (!empty(get_the_title())) { ?>
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <? } ?>
                    <div class="item__img">
                        <?php echo get_the_post_thumbnail($page->ID); 
                        if(get_field('schemes')){
        echo get_field('schemes', false, false);
    }
?>
                        <?
                        if($image) {
                            echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                        }
                        ?>
                    </div>
                    <div class="item__text">

                        <?php
                        global $more;
                        $more = 1;
                        the_content();
                        ?>
                        <?php
                            $table = get_field('tablicza');
                        if ( ! empty ( $table ) ) {

echo '<table border="0">';

    if ( ! empty( $table['caption'] ) ) {

        echo '<caption>' . $table['caption'] . '</caption>';
    }

    if ( ! empty( $table['header'] ) ) {

        echo '<thead>';

            echo '<tr>';

                foreach ( $table['header'] as $th ) {

                    echo '<th>';
                        echo $th['c'];
                    echo '</th>';
                }

            echo '</tr>';

        echo '</thead>';
    }

    echo '<tbody>';

        foreach ( $table['body'] as $tr ) {

            echo '<tr>';

                foreach ( $tr as $td ) {

                    echo '<td>';
                        echo $td['c'];
                    echo '</td>';
                }

            echo '</tr>';
        }

    echo '</tbody>';

echo '</table>';
}

                        if (get_field('opisanie_tovara-en')) {
                            echo get_field('opisanie_tovara-en', false, false);
                        }
                        if($oon["url"]) {
                            echo '<a class="oon" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                        }
                        if($ts["url"]) {
                            echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                        }
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
</div>
<div class="feed container">
    <h2 class="feed__title">Contact us</h2>
    <?php echo do_shortcode('[contact-form-7 id="1166" title="Contact form 2"]'); ?>
</div>
</main>

        <? }
elseif( in_category( 'gas-holders' )){ ?>
<main class="main inner">
<div class="breadcrumbs">
    <div class="breadcrumbs__wrapper container">
        <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
        <span> \ </span>
        <a href="/en/products/">Products</a>
        <span> \ </span>
        <a>Gasholder</a>
    </div>
</div>
<section class="container">
    <h1 class="catalog__title title__head">
        Gasholder
    </h1>

    <nav class="autotank__menu">
        <ul class="menu__list">
            <li class="menu__item active">
            Mini-gasholder
            </li>
            <li class="menu__item">
            Underground gas holders
            </li>
        </ul>
    </nav>
</section>
<div class="container">
    <section class="autotank__tor tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'mini-gas-holders',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="catalog__item">
                    <? if (!empty(get_the_title())) { ?>
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <? } ?>
                    <div class="item__img">
                    <?php echo get_the_post_thumbnail($page->ID); 
                        if(get_field('schemes')){
        echo get_field('schemes', false, false);
    } 
    ?>
    
                    <?php $image = get_field('shemy_kartinka-en');
                            $oon = get_field('ssylka_sertifikaty_oon-en');
                            $ts = get_field('ssylka_sertifikaty_ts-en');
                    if($image) {
                        echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                    }
                    ?>
                    </div>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;
                        the_content();
                        ?>
                        <?php 
                            $table = get_field('tablicza-en');
                        if ( ! empty ( $table ) ) {

echo '<table border="0">';

    if ( ! empty( $table['caption'] ) ) {

        echo '<caption>' . $table['caption'] . '</caption>';
    }

    if ( ! empty( $table['header'] ) ) {

        echo '<thead>';

            echo '<tr>';

                foreach ( $table['header'] as $th ) {

                    echo '<th>';
                        echo $th['c'];
                    echo '</th>';
                }

            echo '</tr>';

        echo '</thead>';
    }

    echo '<tbody>';

        foreach ( $table['body'] as $tr ) {

            echo '<tr>';

                foreach ( $tr as $td ) {

                    echo '<td>';
                        echo $td['c'];
                    echo '</td>';
                }

            echo '</tr>';
        }

    echo '</tbody>';

echo '</table>';

                            if (get_field('opisanie_tovara-en')) {
                                echo get_field('opisanie_tovara-en', false, false);
                            }
                            if($oon["url"]) {
                                echo '<a class="oon" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                            }
                            if($ts["url"]) {
                                echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                            }
}
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
    <section class="autotank__cylindrical tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'underground-gas-holders',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="catalog__item">
                    <? if (!empty(get_the_title())) { ?>
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <? } ?>
                    <div class="item__img">
                        <?php echo get_the_post_thumbnail($page->ID); 
                        if(get_field('schemes')){
        echo get_field('schemes', false, false);
    }
?>
                        <?
                        $image = get_field('shemy_kartinka-en');
                        if($image) {
                            echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                        }
                        ?>
                    </div>
                    <div class="item__text">

                        <?php
                        global $more;
                        $more = 1;
                        the_content();
                        ?>
                        <?php
                            $table = get_field('tablicza-en');
                        if ( ! empty ( $table ) ) {

echo '<table border="0">';

    if ( ! empty( $table['caption'] ) ) {

        echo '<caption>' . $table['caption'] . '</caption>';
    }

    if ( ! empty( $table['header'] ) ) {

        echo '<thead>';

            echo '<tr>';

                foreach ( $table['header'] as $th ) {

                    echo '<th>';
                        echo $th['c'];
                    echo '</th>';
                }

            echo '</tr>';

        echo '</thead>';
    }

    echo '<tbody>';

        foreach ( $table['body'] as $tr ) {

            echo '<tr>';

                foreach ( $tr as $td ) {

                    echo '<td>';
                        echo $td['c'];
                    echo '</td>';
                }

            echo '</tr>';
        }

    echo '</tbody>';

echo '</table>';
}

                        if (get_field('opisanie_tovara-en')) {
                            echo get_field('opisanie_tovara-en', false, false);
                        }
                        if($oon["url"]) {
                            echo '<a class="oon" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                        }
                        if($ts["url"]) {
                            echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                        }
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
</div>
<div class="feed container">
    <h2 class="feed__title">Contact us</h2>
    <?php echo do_shortcode('[contact-form-7 id="1166" title="Contact form 2"]'); ?>
</div>
</main>
	<? 
} elseif ( in_category( 'produkcziya' )) {
    ?>
    <main class="main inner">
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
                <span> \ </span>

                <a href="/produkcziya/"> Продукция </a>
                <span> \ </span>
                <a> <? single_cat_title() ?> </a>
            </div>
        </div>
        <section class="products container">
            <h1 class="products__title title__head"> <? single_cat_title() ?></h1>
            <?php if (have_posts()) : ?>

                <?php
                /* Start the Loop */
                while (have_posts()) :
                    the_post(); ?>
                    <div class="catalog__item">
                        <? the_title('<h2 class="item__title title">', '</h2>'); ?>
                        <div class="item__desc">

                            <?php
                            if (get_field('opisanie_tovara')) {
                                echo get_field('opisanie_tovara', false, true);
                            }
                            ?>
                        </div>
                        <div class="item__img">
                            <?
                            if (has_post_thumbnail()) {
                                the_post_thumbnail();
                            }
                            ?>
                            <!--                   --><?// balcity_ru_post_thumbnail();
                            ?>
                            <!-- <div class="hero">
    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
</div> -->
                            <?php
                            if (get_field('schemes')) {
                                echo get_field('schemes', false, false);
                            }
                            ?>
                            <?php $image = get_field('shemy_kartinka');
                            $oon = get_field('ssylka_sertifikaty_oon');
                            $ts = get_field('ssylka_sertifikaty_ts');
                            if($image) {
                                echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                            }
                            ?>

                        </div>

                        <div class="item__text">

                            <?php $table = get_field('tablicza');
                            if (!empty ($table)) {

                                echo '<table border="0">';

                                if (!empty($table['caption'])) {

                                    echo '<caption>' . $table['caption'] . '</caption>';
                                }

                                if (!empty($table['header'])) {

                                    echo '<thead class="hek">';

                                    echo '<tr>';

                                    foreach ($table['header'] as $th) {

                                        echo '<th>';
                                        echo $th['c'];
                                        echo '</th>';
                                    }

                                    echo '</tr>';

                                    echo '</thead>';
                                }

                                echo '<tbody>';

                                foreach ($table['body'] as $tr) {

                                    echo '<tr>';

                                    foreach ($tr as $td) {

                                        echo '<td>';
                                        echo $td['c'];
                                        echo '</td>';
                                    }

                                    echo '</tr>';
                                }

                                echo '</tbody>';

                                echo '</table>';
                            }
//                            if (get_field('opisanie_tovara')) {
//                                echo get_field('opisanie_tovara', false, false);
//                            }
                            if($oon["url"]) {
                                echo '<a class="oon" href="' . $oon["url"] .'#oon">Официальное утверждение ЕЭК ООН</a>';
                            }
                            if($ts["url"]) {
                                echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">Сертификаты Таможенного Союза</a>';
                            }
                            ?>
                            <?
                            the_content(
                                sprintf(
                                    wp_kses(
                                    /* translators: %s: Name of current post. Only visible to screen readers */
                                        __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'balcity-ru'),
                                        array(
                                            'span' => array(
                                                'class' => array(),
                                            ),
                                        )
                                    ),
                                    wp_kses_post(get_the_title())
                                )
                            );
                            wp_link_pages(
                                array(
                                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'balcity-ru'),
                                    'after' => '</div>',
                                )
                            );
                            ?>
                        </div>
                    </div><!-- .entry-content -->
                <?
                endwhile;

                the_posts_navigation();

            else :

                get_template_part('template-parts/content', 'none');

            endif;
            ?>

        </section>
        <!--    <div class="feed container">-->
        <!--        <h2 class="feed__title">Задать вопрос</h2>-->
        <!--        --><?php //echo do_shortcode('[contact-form-7 id="126" class="contact-us" title="Contact form 1"]'); ?>
        <!--    </div>-->

    </main>
<?
} elseif( in_category( 'automotive-tanks' )){ ?>
<main class="main inner">
<div class="breadcrumbs">
    <div class="breadcrumbs__wrapper container">
        <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
        <span> \ </span>
        <a href="/en/products/">Products</a>
        <span> \ </span>
        <a><? single_cat_title() ?></a>
    </div>
</div>
<section class="container">
    <h1 class="catalog__title title__head">
        <? single_cat_title() ?>
    </h1>

    <nav class="autotank__menu">
        <ul class="menu__list">
            <li class="menu__item active">
            Toroidal tanks
            </li>
            <li class="menu__item">
                Cylindrical tanks
            </li>
            <li class="menu__item">
                Twin tanks
            </li>
        </ul>
    </nav>
</section>
<section class="autotank__about">
    <div class="container">
        <h3 class="about__title"><?php pll_e('Automotive-title'); ?></h3>
        <ul class="about__list">
            <li class="list__item">
                <div class="item__name"><?php pll_e('Automotive-row1'); ?></div>
                <div class="item__val"><?php pll_e('Automotive-value1'); ?></div>
            </li>
            <li class="list__item">
                <div class="item__name"><?php pll_e('Automotive-row2'); ?></div>
                <div class="item__val"><?php pll_e('Automotive-value2'); ?></div>
            </li>
            <li class="list__item">
                <div class="item__name"><?php pll_e('Automotive-row3'); ?></div>
                <div class="item__val"><?php pll_e('Automotive-value3'); ?></div>
            </li>
            <li class="list__item">
                <div class="item__name"><?php pll_e('Automotive-row4'); ?></div>
                <div class="item__val"><?php pll_e('Automotive-value4'); ?></div>
            </li>
        </ul>
        <div class="about__info">
            <img src="<?= get_template_directory_uri() ?>/assets/img/iso.png" alt="" class="about__img">
            <div class="about__text">
            <?php pll_e('Automotive-text'); ?> <a href="/category/sertifikaty/">Certificate</a>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <section class="autotank__tor tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'toroidal-tanks',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="catalog__item">
                    <? if (!empty(get_the_title())) { ?>
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <? } ?>
                    <div class="item__img">
                    <?php echo get_the_post_thumbnail($page->ID); 
                        if(get_field('schemes')){
        echo get_field('schemes', false, false);
    } 
    ?>
                    <?php $image = get_field('shemy_kartinka');
                    
                      if(get_the_ID() == '885') {
                        $image['url'] = '/wp-content/uploads/2021/09/chertezh-tor-vnutrenni.png';
                      }
                            $oon = get_field('ssylka_sertifikaty_oon-en');
                            $ts = get_field('ssylka_sertifikaty_ts-en');
                  
                    if($image) {
                        echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                    }
                    ?>
                    </div>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;
                        the_content();
                        ?>
                        <?php 
                            $table = get_field('tablicza-en');
                        if ( ! empty ( $table ) ) {

echo '<table border="0">';

    if ( ! empty( $table['caption'] ) ) {

        echo '<caption>' . $table['caption'] . '</caption>';
    }

    if ( ! empty( $table['header'] ) ) {

        echo '<thead>';

            echo '<tr>';

                foreach ( $table['header'] as $th ) {

                    echo '<th>';
                        echo $th['c'];
                    echo '</th>';
                }

            echo '</tr>';

        echo '</thead>';
    }

    echo '<tbody>';

        foreach ( $table['body'] as $tr ) {

            echo '<tr>';

                foreach ( $tr as $td ) {

                    echo '<td>';
                        echo $td['c'];
                    echo '</td>';
                }

            echo '</tr>';
        }

    echo '</tbody>';

echo '</table>';

                            if (get_field('opisanie_tovara-en')) {
                                echo get_field('opisanie_tovara-en', false, false);
                            }
                            if($oon["url"]) {
                                echo '<a class="oon" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                            }
                            if($ts["url"]) {
                                echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                            }
}
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
    <section class="autotank__cylindrical tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'cylindrical-tanks',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="catalog__item">
                    <? if (!empty(get_the_title())) { ?>
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <? } ?>
                    <div class="item__img">
                        <?php echo get_the_post_thumbnail($page->ID); 
                        if(get_field('schemes')){
        echo get_field('schemes', false, false);
    }
?>
                        <?
                        if($image) {
                            echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                        }
                        ?>
                    </div>
                    <div class="item__text">

                        <?php
                        global $more;
                        $more = 1;
                        the_content();
                        ?>
                        <?php
                            $table = get_field('tablicza-en');
                        if ( ! empty ( $table ) ) {

echo '<table border="0">';

    if ( ! empty( $table['caption'] ) ) {

        echo '<caption>' . $table['caption'] . '</caption>';
    }

    if ( ! empty( $table['header'] ) ) {

        echo '<thead>';

            echo '<tr>';

                foreach ( $table['header'] as $th ) {

                    echo '<th>';
                        echo $th['c'];
                    echo '</th>';
                }

            echo '</tr>';

        echo '</thead>';
    }

    echo '<tbody>';

        foreach ( $table['body'] as $tr ) {

            echo '<tr>';

                foreach ( $tr as $td ) {

                    echo '<td>';
                        echo $td['c'];
                    echo '</td>';
                }

            echo '</tr>';
        }

    echo '</tbody>';

echo '</table>';
}

                        if (get_field('opisanie_tovara-en')) {
                            echo get_field('opisanie_tovara-en', false, false);
                        }
                        if($oon["url"]) {
                            echo '<a class="oon" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                        }
                        if($ts["url"]) {
                            echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                        }
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
    <section class="autotank__twin tab-item">
        <?php $query = new WP_Query(array(
            'category_name' => 'twin-tanks',
        )); ?>
        <?php if ($query->have_posts()) : $i = 1;
            while ($query->have_posts()) : $query->the_post(); ?>
                <div class="catalog__item">
                    <? if (!empty(get_the_title())) { ?>
                    <h2 class="item__title title"><?php the_title(); ?></h2>
                    <? } ?>
                    <div class="item__img">
                        <?php echo get_the_post_thumbnail($page->ID); 
                        if(get_field('schemes')){
        echo get_field('schemes', false, false);
    }
                        if($image) {
                            echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                        }
?>
                    </div>
                    <div class="item__text">
                        <?php
                        global $more;
                        $more = 1;
                        the_content();

                        $table = get_field('tablicza-en');
                        if (!empty ($table)) {

                            echo '<table border="0">';

                            if (!empty($table['caption'])) {

                                echo '<caption>' . $table['caption'] . '</caption>';
                            }

                            if (!empty($table['header'])) {

                                echo '<thead>';

                                echo '<tr>';

                                foreach ($table['header'] as $th) {

                                    echo '<th>';
                                    echo $th['c'];
                                    echo '</th>';
                                }

                                echo '</tr>';

                                echo '</thead>';
                            }

                            echo '<tbody>';

                            foreach ($table['body'] as $tr) {

                                echo '<tr>';

                                foreach ($tr as $td) {

                                    echo '<td>';
                                    echo $td['c'];
                                    echo '</td>';
                                }

                                echo '</tr>';
                            }

                            echo '</tbody>';

                            echo '</table>';
                        }
                        if (get_field('opisanie_tovara-en')) {
                            echo get_field('opisanie_tovara-en', false, false);
                        }
                            if($oon["url"]) {
                                echo '<a class="oon" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                            }
                            if($ts["url"]) {
                                echo '<a class="ts" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                            }
                        ?>
                    </div>
                </div>
                <?php $i++; endwhile; ?>

        <?php else: ?>
            <!-- no posts found -->
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section>
</div>
<div class="feed container">
    <h2 class="feed__title">Contact us</h2>
    <?php echo do_shortcode('[contact-form-7 id="1166" title="Contact form 2"]'); ?>
</div>
</main>
<?
} elseif( in_category( 'products' )){ ?>
    <main class="main inner">
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
                <span> \ </span>

                <a href="/products/"> Products </a>
                <span> \ </span>
                <a> <? single_cat_title() ?> </a>
            </div>
        </div>
        <section class="products container">
            <h1 class="products__title title__head"> <? single_cat_title() ?></h1>
            <?php if (have_posts()) : ?>

                <?php
                /* Start the Loop */
                while (have_posts()) :
                    the_post(); ?>
                    <div class="catalog__item">
                        <? the_title('<h2 class="item__title title">', '</h2>'); ?>
                        <div class="item__desc">

                            <?php
                            if (get_field('opisanie_tovara-en')) {
                                echo get_field('opisanie_tovara-en', false, true);
                            }
                            ?>
                        </div>
                        <div class="item__img">
                            <?
                            if (has_post_thumbnail()) {
                                the_post_thumbnail();
                            }
                            ?>
                            <!--                   --><?// balcity_ru_post_thumbnail();
                            ?>
                            <?php
                            if (get_field('schemes')) {
                                echo get_field('schemes', false, false);
                            }
                            ?>
                            <?php $image = get_field('shemy_kartinka-en');
                            $oon = get_field('ssylka_sertifikaty_oon-en');
                            $ts = get_field('ssylka_sertifikaty_ts-en');
                            if($image) {
                                echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                            }
                            ?>

                        </div>

                        <div class="item__text">

                            <?php $table = get_field('tablicza-en');
                            if (!empty ($table)) {

                                echo '<table border="0">';

                                if (!empty($table['caption'])) {

                                    echo '<caption>' . $table['caption'] . '</caption>';
                                }

                                if (!empty($table['header'])) {

                                    echo '<thead class="hek">';

                                    echo '<tr>';

                                    foreach ($table['header'] as $th) {

                                        echo '<th>';
                                        echo $th['c'];
                                        echo '</th>';
                                    }

                                    echo '</tr>';

                                    echo '</thead>';
                                }

                                echo '<tbody>';

                                foreach ($table['body'] as $tr) {

                                    echo '<tr>';

                                    foreach ($tr as $td) {

                                        echo '<td>';
                                        echo $td['c'];
                                        echo '</td>';
                                    }

                                    echo '</tr>';
                                }

                                echo '</tbody>';

                                echo '</table>';
                            }
//                            if (get_field('opisanie_tovara')) {
//                                echo get_field('opisanie_tovara', false, false);
//                            }

                            ?>
                            <?
                            the_content(
                                sprintf(
                                    wp_kses(
                                    /* translators: %s: Name of current post. Only visible to screen readers */
                                        __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'balcity-ru'),
                                        array(
                                            'span' => array(
                                                'class' => array(),
                                            ),
                                        )
                                    ),
                                    wp_kses_post(get_the_title())
                                )
                            );
                            wp_link_pages(
                                array(
                                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'balcity-ru'),
                                    'after' => '</div>',
                                )
                            );
                            if($oon["url"]) {
                                echo '<a class="oon" style="margin-top:20px" href="' . $oon["url"] .'#oon">ECE UN approvals</a>';
                            }
                            if($ts["url"]) {
                                echo '<a class="ts" style="margin-top:20px" href="' . $ts["url"] .'#tamogsouz">The Customs Union Certificates</a>';
                            }
                            ?>
                        </div>
                    </div><!-- .entry-content -->
                <?
                endwhile;

                the_posts_navigation();

            endif;
            ?>

        </section>
        <!--    <div class="feed container">-->
        <!--        <h2 class="feed__title">Задать вопрос</h2>-->
        <!--        --><?php //echo do_shortcode('[contact-form-7 id="126" class="contact-us" title="Contact form 1"]'); ?>
        <!--    </div>-->

    </main>
    <?


?>
<?    
} else {
?>
<main class="main inner">
<div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
                <span> \ </span>

                <a href="<?php pll_e('productsLink'); ?>"> <?php pll_e('products'); ?> </a>
                <span> \ </span>
                <a> <? single_cat_title() ?> </a>
            </div>
        </div>
    <div class="container">
      <h1 class="catalog__title title__head"><? single_cat_title() ?></h1>
        <h2 style="margin-top:25px;"><?php pll_e('no-product'); ?></h2>
    </div>
</main>
<?
}
get_footer();
?>