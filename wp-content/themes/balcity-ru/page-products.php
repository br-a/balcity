<?php

get_header();
?>

    <main class="main inner">
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>">Main Page</a>
                <span> \ </span>
                <a><?php the_title(); ?></a>
            </div>
        </div>
        <section class="products container">
            <h1 class="products__title title__head">
                <?php the_title(); ?>
            </h1>
            <div class="products__wrapper">
                <div class="products__item tanks">
                    <h2 class="item__title">Domestic gas bottles</h2>
                    <a href="/en/category/products/domestic-gas-bottles/" class="item__link en">more</a></div>
                <div class="products__item auto-tanks">
                    <h2 class="item__title">Automotive tanks</h2>
                    <a href="/en/category/automotive-tanks/" class="item__link en">more</a></div>
            </div>
            <div class="products__wrapper">
                <div class="products__item gazgolder">
                    <h2 class="item__title">Gasholder</h2>
                    <a href="/en/category/products/gas-holders/" class="item__link en">more</a></div>
                <div class="products__item vent">
                    <h2 class="item__title">Gas shut-off valves</h2>
                    <a href="/en/category/products/gas-shut-off-valves/" class="item__link en">more</a></div>
            </div>
            <div class="products__wrapper">
                <div class="products__item venth2o">
                    <h2 class="item__title">LPG</h2>
                    <a href="/en/category/products/lpg/" class="item__link en">more</a></div>
                <div class="products__item loader">
                    <h2 class="item__title">Tank for a forklift</h2>
                    <a href="/en/category/products/tank-for-a-forklift/" class="item__link en">more</a></div>
            </div>
            <div class="products__wrapper">
                <div class="products__item other">
                    <h2 class="item__title">Other products</h2>
                    <a href="/en/category/products/other-products/" class="item__link en">more</a></div>
            </div>
    </main><!-- #main -->

<?php
//get_sidebar();
get_footer();
