<?php get_header(); ?>
<main class="main inner">
    <div class="breadcrumbs">
        <div class="breadcrumbs__wrapper container">
            <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
            <span> \ </span>
            <a href="/en/category/certificate/">Certificate</a>
            <span> \ </span>
            <a> <? the_title() ?> </a>
        </div>
    </div>
	<? 
if ( in_category( 'certificate' )) {
    ?>
<section class="cert container">
        <h1 class="cert__title title__head">
            <? the_title() ?>
        </h1>
        <div class="cert__info">
            <div class="cert__img">
                <img src="<?= get_template_directory_uri() ?>/assets/img/cert-info.png" alt="">
            </div>

            <div class="cert__text">
                Соответствие изготавливаемой предприятием продукции для сжиженного углеводородного газа требованиям безопасности при разработке и производстве обеспечено выполнением Правил ЕЭК ООН № 67–01 и технических регламентов Таможенного союза (ТР ТС 018/2011, ТР ТС 032/2013) и приказа Ростехнадзора от 25.03.2014 года № 116 в форме обязательной сертификации.
                Система менеджмента качества предприятия применительно к производству, продаже и поставке продукции сертифицирована на соответствие ГОСТ ISO 9001–2015.
            </div>
        </div>
        <nav class="cert__menu">
            <ul class="menu__list">
                <li class="menu__item active">
                    All
                </li>
                <li class="menu__item" id="ts-menu">
				The Customs Union Certificates
                </li>
                <li class="menu__item" id="oon-menu">
				ECE UN approvals
                </li>
            </ul>
        </nav>
    </section>
    <div class="cert__wrapper">
        <section class="cert__all tab-item">
                    <div class="cert__item">
                        <div class="container">
                            <div class="item__text">
                                <?php
                                $countEmpty = 0;
                                $rowsTs = get_field('sertifikaty_ts-en');
                                $rowsOon = get_field('sertifikaty_oon-en');
                                if(!empty($rowsTs) || !empty($rowsOon)) { // если хотябы 1 не пустой
                                    if(!empty($rowsTs) && !empty($rowsOon)) { // если оба не пустые
                                        $rowsAll = array_merge($rowsTs, $rowsOon);
                                    }
                                    else $rowsAll = !empty($rowsTs) ? $rowsTs : $rowsOon;
                            
                                foreach ($rowsAll as $row) {

                                        $link = $row['sertifikaty_ts-link-en'] ? $row['sertifikaty_ts-link-en'] : $row['sertifikaty_oon-en'];
                                        if($link) {
                                            echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                        } else {
                                                $countEmpty++;
                                        }
                                }
                                if(count($rowsAll) == $countEmpty) echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>'. $countEmpty . ' ' . count($rowsAll);

                            } else echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                                ?>
                            </div>
                        </div>
                    </div>
        </section>
        <section class="cert__oon tab-item" id="ts">
                    <div class="cert__item">
                        <div class="container">
                            <div class="item__text">
                            <?php
                                $countEmpty = 0;
                                $rows = get_field('sertifikaty_ts-en');
                                if ($rows) {
                                    foreach ($rows as $row) {
                                        $link = $row['sertifikaty_ts-link-en'];
                                        if($link) {
                                            echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                            }
                                            else {
                                                $countEmpty++;
                                            }
                                    }
                                    if(count($rows) == $countEmpty) {
                                        echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                                    }
                                }
                                else {
                                    echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                                }
                                ?>

                            </div>
                        </div>
                    </div>
        </section>
        <section class="cert__ts tab-item" id="oon">
                    <div class="cert__item">
                        <div class="container">
                            <div class="item__text">
                            <?php
                                $countEmpty = 0;
                                $rows = get_field('sertifikaty_oon-en');
                                if ($rows) {
                                    foreach ($rows as $row) {
                                        $link = $row['sertifikaty_oon-en'];
                                        if($link) {
                                            echo '<a href="' . $link['url'] . '">' . $link['title'] . '</a>';
                                        } 
                                        else {
                                            $countEmpty++;
                                        }
                                    }
                                    if(count($rows) == $countEmpty) {
                                        echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                                    }
                                }
                                else {
                                    echo '<p style="margin-bottom: 30px;">No certficates found for this category</p>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
        </section>
		<? 
			}
		?>
    </div>
</main>
<?php get_footer(); ?>
<script>
    console.log(window.location.href);
    if ((window.location.href).includes('#oon') ) {
        $('.tab-item').hide();
        $('#oon').show();
        $('.cert__menu .menu__item').removeClass('active');
        $('#oon-menu').addClass('active');
    } else if((window.location.href).includes('#tamogsouz') ) {
        $('.tab-item').hide();
        $('#ts').show();
        $('.cert__menu .menu__item').removeClass('active');
        $('#ts-menu').addClass('active');
    }

</script>