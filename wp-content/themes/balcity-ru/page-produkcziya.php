<?php

get_header();
?>

    <main class="main inner">
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>">Главная</a>
                <span> \ </span>
                <a><?php the_title(); ?></a>
            </div>
        </div>
        <section class="products container">
            <h1 class="products__title title__head">
                <?php the_title(); ?>
            </h1>
            <div class="products__wrapper">
                <div class="products__item tanks">
                    <h2 class="item__title">Бытовые
                        баллоны</h2>
                    <a href="/category/produkcziya/bytovye-ballony/" class="item__link">подробнее</a></div>
                <div class="products__item auto-tanks">
                    <h2 class="item__title">Автомобильные баллоны</h2>
                    <a href="/category/avtomobilnye-ballony/" class="item__link">подробнее</a></div>
            </div>
            <div class="products__wrapper">
                <div class="products__item gazgolder">
                    <h2 class="item__title">Газгольдеры</h2>
                    <a href="/category/produkcziya/gazgoldery/" class="item__link">подробнее</a></div>
                <div class="products__item vent">
                    <h2 class="item__title">Запорные вентили
                        для газа</h2>
                    <a href="/category/produkcziya/zapornye-ventili-dlya-gaza/" class="item__link">подробнее</a></div>
            </div>
            <div class="products__wrapper">
                <div class="products__item venth2o">
                    <h2 class="item__title">Вентили
                        для кислорода</h2>
                    <a href="/category/produkcziya/ventil-dlya-kisloroda/" class="item__link">подробнее</a></div>
                <div class="products__item loader">
                    <h2 class="item__title">Баллоны для погрузчика</h2>
                    <a href="/category/produkcziya/ballony-dlya-pogruzchika/" class="item__link">подробнее</a></div>
            </div>
            <div class="products__wrapper">
                <div class="products__item other">
                    <h2 class="item__title">Другая продукция</h2>
                    <a href="/category/produkcziya/drugaya-produkcziya/" class="item__link">подробнее</a></div>
            </div>
    </main><!-- #main -->

<?php
//get_sidebar();
get_footer();
