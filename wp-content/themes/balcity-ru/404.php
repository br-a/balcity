<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package balcity.ru
 */

get_header();
?>

	<main class="main inner">

		<section class="error-404 not-found container">
            <h1 class="title__head"><?php pll_e('title-404'); ?></h1>
            <div class="not-found__text">
            <?php pll_e('text-404'); ?>
                <p>
                <a href="/"><a href="<?php pll_e('main-link'); ?>"><?php pll_e('link-404'); ?></a>.
            </p>
            </div>

<!--			<header class="page-header">-->
<!--				<h1 class="page-title">--><?php //esc_html_e( 'Oops! That page can&rsquo;t be found.', 'balcity-ru' ); ?><!--</h1>-->
<!--			</header><!-- .page-header -->

<!--			<div class="page-content">-->
<!--				<p>--><?php //esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'balcity-ru' ); ?><!--</p>-->
<!---->
<!--					--><?php
//					get_search_form();
//
//					the_widget( 'WP_Widget_Recent_Posts' );
//					?>
<!---->
<!--					<div class="widget widget_categories">-->
<!--						<h2 class="widget-title">--><?php //esc_html_e( 'Most Used Categories', 'balcity-ru' ); ?><!--</h2>-->
<!--						<ul>-->
<!--							--><?php
//							wp_list_categories(
//								array(
//									'orderby'    => 'count',
//									'order'      => 'DESC',
//									'show_count' => 1,
//									'title_li'   => '',
//									'number'     => 10,
//								)
//							);
//							?>
<!--						</ul>-->
<!--					</div><!-- .widget -->
<!---->
<!--					--><?php
//					/* translators: %1$s: smiley */
//					$balcity_ru_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'balcity-ru' ), convert_smilies( ':)' ) ) . '</p>';
//					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$balcity_ru_archive_content" );
//
//					the_widget( 'WP_Widget_Tag_Cloud' );
//					?>
<!---->
<!--			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
