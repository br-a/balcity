<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package balcity.ru
 */

get_header();
?>
    <main class="main inner">
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>">Главная</a>
                <span> \ </span>
                <a><?php the_title(); ?></a>
            </div>
        </div>
        <section class="company container">
            <h1 class="company__title title__head">
                <?php the_title(); ?>
            </h1>
            <!-- .entry-header -->
            <div class="company__wrapper">
                <div class="company__prod">
                    <div class="prod__img">
                    <?php balcity_ru_post_thumbnail(); ?>
                    </div> 
                    <div class="prod__text"> 
                <?php
                the_content();

                wp_link_pages(
                    array(
                        'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'balcity-ru' ),
                        'after'  => '</div>',
                    )
                );
                ?>
                </div>
                </div>
                <div class="company__text">
                <?php
        if(get_field('tekst_posle_osnovnogo')){
            echo '<p>'.get_field('tekst_posle_osnovnogo').'</p>';
        }
    ?>
                </div>
            </div><!-- .entry-content -->
        </section>
        <div class="company__standart container">
            <img src="<?= get_template_directory_uri() ?>/assets/img/group-28.png" alt="">
        <?php
        if(get_field('seryj_kontejner')){
            echo '<p>'.get_field('seryj_kontejner').'</p>';
        }
    ?>
                    </div>
        </div>
        <div class="company__text--down container">
        <?php
        if(get_field('tekst_vnizu')){
            echo '<p>'.get_field('tekst_vnizu').'</p>';
        }
    ?>
        </div>
    </main><!-- #main -->

<?php

get_footer();