<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package balcity.ru
 */

get_header();
?>
    <main class="main inner">
    <?php
            //$galert = get_fields('galj');
             $slajder = get_field('slajder-gal', 55);
             
            //  var_dump($slajder);
            // var_dump($galert);
            //     foreach($slajder  as $item => $value) {
            //         echo "<img src='" . $slajder ['izobrazhenie']['url'] ."' >";
            //     }
            ?>
        <div class="breadcrumbs">
            <div class="breadcrumbs__wrapper container">
                <a href="<?php echo get_home_url(); ?>">Main</a>
                <span> \ </span>
                <a><?php the_title(); ?></a>
            </div>
        </div>
        <section class="gallery container">
            <h1 class="gallery__title title__head">
                <?php the_title(); ?>
            </h1>
<!-- .entry-header -->

	<?php balcity_ru_post_thumbnail(); ?>

	<div class="gallery__wrapper desc">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'balcity-ru' ),
				'after'  => '</div>',
			)
		);
		?>
    </div>
    <div class="gallery__wrapper mobile">
        <?
    foreach($slajder as $item => $value) { ?>
            <div class="gallery__item">
                <img src="<? echo $slajder[$item]['izobrazhenie']['url']; ?>" >
            </div>  
            <? } ?>  
            
        </div>

    <!-- .entry-content -->
            <!-- <article class="gallery__wrapper">
                <div class="gallery__row">
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/gallerym.jpg" alt="" class="item__img"></div>
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/galleryb.jpg" alt="" class="item__img"></div>
                </div>
                <div class="gallery__row">
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/gallerym.jpg" alt="" class="item__img"></div>
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/galleryb.jpg" alt="" class="item__img"></div>
                </div>
                <div class="gallery__row">
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/gallerym.jpg" alt="" class="item__img"></div>
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/galleryb.jpg" alt="" class="item__img"></div>
                </div>
                <div class="gallery__row">
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/gallerym.jpg" alt="" class="item__img"></div>
                    <div class="gallery__item"><img src="<?= get_template_directory_uri() ?>/assets/img/galleryb.jpg" alt="" class="item__img"></div>
                </div> -->
        </section>
    </main><!-- #main -->
<script>
$( document ).ready(function() {
    $('.mobile').slick({
        infinite: true,
	    dots: true
    });
});

</script>
<?php

get_footer();