<?php get_header(); ?>
<main class="main inner">
    <div class="breadcrumbs">
        <div class="breadcrumbs__wrapper container">
            <a href="<?php echo get_home_url(); ?>"><?php pll_e('main-page'); ?></a>
            <span> \ </span>
            <a href="/produkcziya/">Продукция</a>
            <span> \ </span>
            <a><? single_cat_title() ?></a>
        </div>
    </div>
    <section class="container">
        <h1 class="catalog__title title__head">
            <? single_cat_title() ?>
        </h1>

        <nav class="autotank__menu">
            <ul class="menu__list">
                <li class="menu__item active">
                    Тороидальные
                    баллоны
                </li>
                <li class="menu__item">
                    Цилиндрические баллоны
                </li>
                <li class="menu__item">
                    Спаренные баллоны
                </li>
            </ul>
        </nav>
    </section>
    <section class="autotank__about">
        <div class="container">
            <h3 class="about__title"><?php pll_e('Automotive-title'); ?></h3>
            <ul class="about__list">
                <li class="list__item">
                    <div class="item__name"><?php pll_e('Automotive-row1'); ?></div>
                    <div class="item__val"><?php pll_e('Automotive-value1'); ?></div>
                </li>
                <li class="list__item">
                    <div class="item__name"><?php pll_e('Automotive-row2'); ?></div>
                    <div class="item__val"><?php pll_e('Automotive-value2'); ?></div>
                </li>
                <li class="list__item">
                    <div class="item__name"><?php pll_e('Automotive-row3'); ?></div>
                    <div class="item__val"><?php pll_e('Automotive-value3'); ?></div>
                </li>
                <li class="list__item">
                    <div class="item__name"><?php pll_e('Automotive-row4'); ?></div>
                    <div class="item__val"><?php pll_e('Automotive-value4'); ?></div>
                </li>
            </ul>
            <div class="about__info">
                <img src="<?= get_template_directory_uri() ?>/assets/img/iso.png" alt="" class="about__img">
                <div class="about__text">
                    Проектирование, производство и контроль качества баллонов выполняется в соответствии с требованиями
                    ISO 9001:2008. <a href="/category/sertifikaty/">Сертификаты</a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <section class="autotank__tor tab-item">
            <?php $query = new WP_Query(array(
                'category_name' => 'toroidalnye-ballony',
            )); ?>
            <?php if ($query->have_posts()) : $i = 1;
                while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="catalog__item">
                        <? if (!empty(get_the_title())) { ?>
                        <h2 class="item__title title"><?php the_title(); ?></h2>
                        <? } ?>
                        <div class="item__img">
                        <?php echo get_the_post_thumbnail($page->ID); 
                            if(get_field('schemes')){
            echo get_field('schemes', false, false);
        } 
        ?>
                        <?php $image = get_field('shemy_kartinka');
                        $oon = get_field('ssylka_sertifikaty_oon');
                        $ts = get_field('ssylka_sertifikaty_ts');
                        if($image) {
                            echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                        }
                        ?>
                        </div>
                        <div class="item__text">
                            <?php
                            global $more;
                            $more = 1;
                            the_content();
                            ?>
							<?php 
								$table = get_field('tablicza');
							if ( ! empty ( $table ) ) {

    echo '<table border="0">';

        if ( ! empty( $table['caption'] ) ) {

            echo '<caption>' . $table['caption'] . '</caption>';
        }

        if ( ! empty( $table['header'] ) ) {

            echo '<thead>';

                echo '<tr>';

                    foreach ( $table['header'] as $th ) {

                        echo '<th>';
                            echo $th['c'];
                        echo '</th>';
                    }

                echo '</tr>';

            echo '</thead>';
        }

        echo '<tbody>';

            foreach ( $table['body'] as $tr ) {

                echo '<tr>';

                    foreach ( $tr as $td ) {

                        echo '<td>';
                            echo $td['c'];
                        echo '</td>';
                    }

                echo '</tr>';
            }

        echo '</tbody>';

    echo '</table>';

                                if (get_field('opisanie_tovara')) {
                                    echo get_field('opisanie_tovara', false, false);
                                }
                                if($oon) {
                                    echo '<a class="oon" href="' . $oon .'#oon">Официальное утверждение ЕЭК ООН</a>';
                                }
                                if($ts) {
                                    echo '<a class="ts" href="' . $ts .'#tamogsouz">Сертификаты Таможенного Союза</a>';
                                }

}
							?>
                        </div>
                    </div>
                    <?php $i++; endwhile; ?>

            <?php else: ?>
                <!-- no posts found -->
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </section>
        <section class="autotank__cylindrical tab-item">
            <?php $query = new WP_Query(array(
                'category_name' => 'czilindricheskie-ballony',
            )); ?>
            <?php if ($query->have_posts()) : $i = 1;
                while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="catalog__item">
                        <? if (!empty(get_the_title())) { ?>
                        <h2 class="item__title title"><?php the_title(); ?></h2>
                        <? } ?>
                        <div class="item__img">
                            <?php echo get_the_post_thumbnail($page->ID); 
                            if(get_field('schemes')){
            echo get_field('schemes', false, false);
        }
    ?>
                            <?
                            if($image) {
                                echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                            }
                            ?>
                        </div>
                        <div class="item__text">

                            <?php
                            global $more;
                            $more = 1;
                            the_content();
                            ?>
							<?php
								$table = get_field('tablicza');
							if ( ! empty ( $table ) ) {

    echo '<table border="0">';

        if ( ! empty( $table['caption'] ) ) {

            echo '<caption>' . $table['caption'] . '</caption>';
        }

        if ( ! empty( $table['header'] ) ) {

            echo '<thead>';

                echo '<tr>';

                    foreach ( $table['header'] as $th ) {

                        echo '<th>';
                            echo $th['c'];
                        echo '</th>';
                    }

                echo '</tr>';

            echo '</thead>';
        }

        echo '<tbody>';

            foreach ( $table['body'] as $tr ) {

                echo '<tr>';

                    foreach ( $tr as $td ) {

                        echo '<td>';
                            echo $td['c'];
                        echo '</td>';
                    }

                echo '</tr>';
            }

        echo '</tbody>';

    echo '</table>';
}

                            if (get_field('opisanie_tovara')) {
                                echo get_field('opisanie_tovara', false, false);
                            }if($oon) {
                                echo '<a class="oon" href="' . $oon .'#oon">Официальное утверждение ЕЭК ООН</a>';
                            }
                            if($ts) {
                                echo '<a class="ts" href="' . $ts .'#tamogsouz">Сертификаты Таможенного Союза</a>';
                            }
							?>
                        </div>
                    </div>
                    <?php $i++; endwhile; ?>

            <?php else: ?>
                <!-- no posts found -->
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </section>
        <section class="autotank__twin tab-item">
            <?php $query = new WP_Query(array(
                'category_name' => 'sparennye-ballony',
            )); ?>
            <?php if ($query->have_posts()) : $i = 1;
                while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="catalog__item">
                        <? if (!empty(get_the_title())) { ?>
                        <h2 class="item__title title"><?php the_title(); ?></h2>
                        <? } ?>
                        <div class="item__img">
                            <?php echo get_the_post_thumbnail($page->ID); 
                            if(get_field('schemes')){
            echo get_field('schemes', false, false);
        }
                            if($image) {
                                echo '<a href="'. $image['url'] .'"><img src="'. $image['url']. '"></a>';
                            }
    ?>
                        </div>
                        <div class="item__text">
                            <?php
                            global $more;
                            $more = 1;
                            the_content();

                            $table = get_field('tablicza');
                            if (!empty ($table)) {

                                echo '<table border="0">';

                                if (!empty($table['caption'])) {

                                    echo '<caption>' . $table['caption'] . '</caption>';
                                }

                                if (!empty($table['header'])) {

                                    echo '<thead>';

                                    echo '<tr>';

                                    foreach ($table['header'] as $th) {

                                        echo '<th>';
                                        echo $th['c'];
                                        echo '</th>';
                                    }

                                    echo '</tr>';

                                    echo '</thead>';
                                }

                                echo '<tbody>';

                                foreach ($table['body'] as $tr) {

                                    echo '<tr>';

                                    foreach ($tr as $td) {

                                        echo '<td>';
                                        echo $td['c'];
                                        echo '</td>';
                                    }

                                    echo '</tr>';
                                }

                                echo '</tbody>';

                                echo '</table>';
                            }
                            if (get_field('opisanie_tovara')) {
                                echo get_field('opisanie_tovara', false, false);
                            }
                            if($oon) {
                                echo '<a class="oon" href="' . $oon .'#oon">Официальное утверждение ЕЭК ООН</a>';
                            }
                            if($ts) {
                                echo '<a class="ts" href="' . $ts .'#tamogsouz">Сертификаты Таможенного Союза</a>';
                            }
                            ?>
                        </div>
                    </div>
                    <?php $i++; endwhile; ?>

            <?php else: ?>
                <!-- no posts found -->
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </section>
    </div>
    <div class="feed container">
        <h2 class="feed__title">Задать вопрос</h2>
        <?php echo do_shortcode('[contact-form-7 id="126" class="contact-us" title="Contact form 1"]'); ?>
    </div>
</main>
<?php get_footer(); ?>
