<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package balcity.ru
 */

?>


<footer class="footer">
    <div class="footer__wrapper container">
        <img src="<?php pll_e('logo-footer'); ?>" alt="" class="footer__logo">
            <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'menu-2',
                )
            );
            ?>

        <div class="footer__contact">
            <div class="contact__address">
               <?php pll_e('addr'); ?>
            </div>
            <div class="contact__tel">
                <div class="tel__item">
                    <a href="tel:+7(495) 955-43-77" class="tel__item--link"> +7(495) 955-43-77</a>
                </div>
                <div class="tel__item">
                    <a href="tel:+7(495) 640-17-86" class="tel__item--link">+7(495) 640-17-86</a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="footer__reserved container">
            © <?php pll_e('Balcity'); ?>™ <?php echo date('Y');?>. All Rights Reserved
        </div>
    </div>
</footer>
<!--	<footer id="colophon" class="site-footer">-->
<!--		<div class="site-info">-->
<!--			<a href="--><?php //echo esc_url( __( 'https://wordpress.org/', 'balcity-ru' ) ); ?><!--">-->
<!--				--><?php
//				/* translators: %s: CMS name, i.e. WordPress. */
//				printf( esc_html__( 'Proudly powered by %s', 'balcity-ru' ), 'WordPress' );
//				?>
<!--			</a>-->
<!--			<span class="sep"> | </span>-->
<!--				--><?php
//				/* translators: 1: Theme name, 2: Theme author. */
//				printf( esc_html__( 'Theme: %1$s by %2$s.', 'balcity-ru' ), 'balcity-ru', '<a href="http://underscores.me/">Underscores.me</a>' );
//				?>
<!--		</div><!-- .site-info -->
<!--	</footer><!-- #colophon -->
<!--</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
