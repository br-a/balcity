<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package balcity.ru
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/temp/temp.css" type="text/css"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
    <header class="header">
        <div class="header__wrapper container">
            <a href="<?php echo get_home_url(); ?>" class="header__logo">
                <img src="<?php pll_e('logo-head'); ?>" alt="" class="logo__img">
<!--                --><?php //the_custom_logo( $blog_id ); ?>
            </a>
            <div class="header__inner">
                <div class="header__contacts">
                    <a class="header__address">
                        <span class="address__link"><?php pll_e('addr'); ?></span>
                        <span class="address__link-mobile"><span class="red">Я</span>ндекс карты</span>
                    </a>
                    <a href="tel:+7(495) 955-43-77" class="header__tel">
                        +7(495) 955-43-77
                    </a>
                    <div class="header__lang">
                    <? wp_nav_menu(
                        array(
                            'theme_location' => 'menu-lang',
                        )
                    );
                    ?>
                    </div>
                    <div class="header__burger"></div>
                </div>
                <nav class="header__menu">
                    <div class="header__call">
                        <a href="" class="call__number">+7(495) 955-43-77</a>
                        <a href="" class="call__button"><?php pll_e('call'); ?></a>
                    </div>
                    <div class="menu__close"></div>
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'walker'         => new WPSE_78121_Sublevel_Walker
                        )
                    );
                    ?>
                </nav>
            </div>
        </div>
    </header>
<script>
    if($(".submenu__list .item__link").hasClass("current"))
        $('.submenu__list').parent().parent().addClass('current');


    // $( #menu-menyu-vybora-yazyka)
    $("#menu-menyu-vybora-yazyka .item__link [alt='English']").parent().addClass('en');
</script>