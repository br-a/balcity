<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package balcity.ru
 */

get_header();
?>
    <main class="main">
        <section class="slider">
            <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-1.jpg);background-size: cover; background-position: center; background-repeat: no-repeat;">
                <div class="item__info container">
                    <h2 class="item__title">
                        Экологичный
                        и безопасный
                    </h2>
                    <div class="item__text">
                        «Балсити» — признанный лидер на российском рынке автомобильных баллонов для сжиженного углеводородного газа
                    </div>
                </div>
            </div>
            <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-2.png);background-size: cover; background-position: center; background-repeat: no-repeat;">
                <div class="item__info container">
                    <h2 class="item__title">
                        Ваш поставщик
                        решений для сжиженного
                        углеводородного газа
                    </h2>
                    <div class="item__text"></div>
                </div>
            </div>
            <div class="slider__item" style="background: url(<?= get_template_directory_uri() ?>/assets/img/slide-4.png); background-size: cover; background-position: center; background-repeat: no-repeat;">
                <div class="item__info container">
                    <h2 class="item__title">
                        Сделано с любовью
                        в России
                    </h2>
                    <div class="item__text"></div>
                </div>
            </div>
        </section>
        <section class="catalog">
            <div class="catalog__wrapper container">
                <div class="catalog__item">
                    <h3 class="item__title">
                        Бытовые
                        баллоны
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/tanks.webp" alt="" class="item__img">
                    <a href="" class="item__link">
                        в каталог
                    </a>
                </div>
                <div class="catalog__item">
                    <h3 class="item__title">
                        Автомобильные
                        баллоны
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/cylinders.webp" alt="" class="item__img">
                    <a href="" class="item__link">
                        в каталог
                    </a>
                </div>
                <div class="catalog__item">
                    <h3 class="item__title">
                        Газгольдеры
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/gas.webp" alt="" class="item__img">
                    <a href="" class="item__link">
                        в каталог
                    </a>
                </div>
                <div class="catalog__item">
                    <h3 class="item__title">
                        Запорные
                        вентили
                    </h3>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/arm.webp" alt="" class="item__img">
                    <a href="" class="item__link">
                        в каталог
                    </a>
                </div>
            </div>
        </section>
        <section class="quality container">
            <img src="<?= get_template_directory_uri() ?>/assets/img/quality.webp" alt="" class="quality__img">
            <div class="quality__text">
                <p>ООО «Балсити» опирается на опыт и сложившиеся традиции в области разработки,производства и эксплуатации баллонов и ёмкостей для сжиженного углеводородного газа (пропана, бутана и их смесей) и занимает лидирующее положение на российском рынке.
                </p>
                <p>
                    Наша компания имеет <a href="">Разрешение Ростехнадзора РФ</a> на применение и сертификаты соответствия на производимую продукцию, серийно изготавливает и поставляет потребителям автомобильные баллоны, ёмкости для хранения СУГ, бытовые баллоны, а также запорные вентили к ним.
                </p>
                <p>
                    Автомобильные баллоны «Балсити» сертифицированы в соответствии с международными Правилами <a href="">ЕЭК ООН 67–01</a>.
                </p>
            </div>
        </section>
        <section class="choose container">
            <h2 class="choose__title">НАС ВЫБИРАЮТ:</h2>
            <ul class="choose__list">
                <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/1.webp" alt="" class="item__img"></li>
                <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/2.webp" alt="" class="item__img"></li>
                <li class="choose__item"><img src="<?= get_template_directory_uri() ?>/assets/img/3.webp" alt="" class="item__img"></li>
            </ul>
            <div class="choose__text">
                ООО «Балсити» является эксклюзивным поставщиком автомобильных газовых баллонов на конвейеры «Горьковского автомобильного завода», «Павловского автобусного завода», а также «Ульяновского автомобильного завода».
            </div>
        </section>
        <section class="goods">
            <h2 class="goods__title">«БАЛСИТИ» в цифрах:</h2>
            <div class="goods__wrapper container">
                <div class="goods__item">
                    <div class="item__count">15<span>лет</span></div>
                    <div class="item__text">производим баллоны  и ёмкости для сжиженного
                        углеводородного газа </div>
                </div>
                <div class="goods__item">
                    <div class="item__count">150</div>
                    <div class="item__text">                опытных
                        специалистов</div>
                </div>
                <div class="goods__item">
                    <div class="item__count">7000 <span>м2</span></div>
                    <div class="item__text">                производственных
                        площадей</div>
                </div>
                <div class="goods__item">
                    <div class="item__count">2 <span>млн</span></div>
                    <div class="item__text">
                        произведённых
                        ёмкостей объёмом
                        от 30 до 10 000 л
                    </div>
                </div>
            </div>
            <video autoplay muted loop class="goods__video">
                <source class="goods__info">
                <source src="<?= get_template_directory_uri() ?>/assets/videoplayback.mp4" type="video/mp4">
            </video>
        </section>
        <section class="about container">
            <img src="<?= get_template_directory_uri() ?>/assets/img/lpg.webp" alt="" class="about__lpg">
            <div class="about__vision">
                <div class="vision__text">
                    <h3 class="vision__title">
                        Видение
                    </h3>
                    В условиях быстро меняющегося мира мы рады предложить потребителям продукцию, которая в полной мере удовлетворяет их потребности и в то же время вносит вклад в сохранность окружающей среды, не ставя под угрозу благосостояние будущих поколений
                </div>
            </div>
            <img src="<?= get_template_directory_uri() ?>/assets/img/worker.webp" alt="" class="about__worker">
            <div class="about__mission">
                <div class="mission__text">
                    <h3 class="mission__title">
                        Миссия
                    </h3>
                    Производить качественную и надёжную продукцию по разумным ценам и способствовать достижению благосостояния людей и их счастья
                </div>
            </div>
        </section>
        <section class="advantage container">
            <h2 class="advantage__title title">Преимущества «Балсити»</h2>
            <div class="advantage__wrapper">
                <div class="advantage__item exp">
                    <h3 class="item__title">
                        Опыт
                    </h3>
                    <div class="item__text">
                        ООО «Балсити» присутствует на рынке газобалонного оборудования уже более 15 лет. За это время мы накопили необходимые компетенции и совершенствовали технологию производства. Непрекращающаяся модернизация производства и акцент на улучшение качества продукции сделали нас одним из передовых игроков на рынке.
                    </div>
                </div>
                <div class="advantage__item iso">
                    <h3 class="item__title">
                        Качество
                    </h3>
                    <div class="item__text">
                        Контроль качества происходит на всех этапах производства, от заказа сырья и материалов до передачи на склад готовой продукции.
                        Учитывая специфику выпускаемых изделий, приоритетной задачей в работе БАЛСИТИ является обеспечение надлежащего качества продукции. Все изделия в процессе изготовления проходят полный комплекс испытаний и подвергаются 100% контролю (рентгеновский, гидравлический и пневматический). 
                        <p>
                            Система менеджмента качества предприятия применительно к производству, продаже и поставке продукции сертифицирована на соответствие Европейскому стандарту ISO 9001–2015.
                            «Балсити» - первая компания-производитель из стран СНГ, получившая Европейские сертификаты ECE R 67–01 на свои автомобильные баллоны.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage__wrapper">
                <div class="advantage__item tech">
                    <h3 class="item__title">
                        Технологии
                    </h3>
                    <div class="item__text">
                        Продукция ООО «Балсити» производится на высокотехнологичном итальянском оборудовании.
                        ООО «Балсити» уделяет особое внимание модернизации производства, что позволяет удовлетворять потребности потребителя и обеспечивать высокое качество продукции.
                        <p>
                            Вот уже на протяжении 15 лет ООО «Балсити» сотрудничает с итальянскими компаниями-поставщиками оборудования для металлообработки. Среди них такие имена, как FACCIN S.p.A., GALDABINI SPA, MECOME Srl, Roccia Srl, MG Srl, GNUTTI Transfer S.p.A, Mecolpress S.p.A., Lucas Srl. Работники нашего предприятия имеют возможность перенимать опыт иностранных коллег.
                            Инвестируя в передовые технологии, мы инвестируем в удовлетворение потребителя от нашей продукции.
                        </p>
                    </div>
                </div>
                <div class="advantage__item eco">
                    <h3 class="item__title">
                        Экологичность
                    </h3>
                    <div class="item__text">
                        Продукция БАЛСИТИ напрямую способствует использованию чистого топлива, будь то автомобильные баллоны, бытовые баллоны, или ёмкости для хранения газа. Делая выбор в пользу сжиженного углеводородного газа, вы тем самым заботитесь о чистоте воздуха в Вашем городе. Давайте заботиться о нашей планете вместе!
                    </div>
                </div>
            </div>
        </section>
    </main>
<!--	<main id="primary" class="site-main">-->
<!---->
<!--		--><?php
//		if ( have_posts() ) :
//
//			if ( is_home() && ! is_front_page() ) :
//				?>
<!--				<header>-->
<!--					<h1 class="page-title screen-reader-text">--><?php //single_post_title(); ?><!--</h1>-->
<!--				</header>-->
<!--				--><?php
//			endif;
//
//			/* Start the Loop */
//			while ( have_posts() ) :
//				the_post();
//
//				/*
//				 * Include the Post-Type-specific template for the content.
//				 * If you want to override this in a child theme, then include a file
//				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
//				 */
//				get_template_part( 'template-parts/content', get_post_type() );
//
//			endwhile;
//
//			the_posts_navigation();
//
//		else :
//
//			get_template_part( 'template-parts/content', 'none' );
//
//		endif;
//		?>

<!--	</main> #main -->

<?php
//get_sidebar();
get_footer();
